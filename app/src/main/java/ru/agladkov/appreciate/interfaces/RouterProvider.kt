package ru.agladkov.appreciate.interfaces

import ru.terrakok.cicerone.Router

/**
 * Created by neura on 09.03.18.
 */
interface RouterProvider {
    fun getRouter(): Router
}