package ru.agladkov.appreciate.interfaces

/**
 * Created by neura on 17.03.18.
 */
interface RedrawHandler {
    fun refreshCurrentFragment()
}