package ru.agladkov.appreciate.interfaces

/**
 * Created by neura on 10.03.18.
 */
interface KeyboardHandler {
    fun hideKeyboard()
    fun showKeyboard()
}