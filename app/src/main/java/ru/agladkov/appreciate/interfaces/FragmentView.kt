package ru.agladkov.appreciate.interfaces

import android.os.Bundle

/**
 * Created by neura on 09.03.18.
 */
interface FragmentView {
    fun exitApp()
    fun updateActionBar(isBack: Boolean, isHistory: Boolean, title: String)
    fun navigateTo(screenKey: String, data: Bundle?)
}