package ru.agladkov.appreciate.interfaces;

/**
 * Created by agladkov on 17.01.18.
 */

public interface MapInterface {
    void crossMiddle(int state);
    void onRelease(int state);
    void onHold();
}
