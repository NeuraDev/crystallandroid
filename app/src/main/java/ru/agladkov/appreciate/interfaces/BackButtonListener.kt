package ru.agladkov.appreciate.interfaces

/**
 * Created by neura on 09.03.18.
 */
interface BackButtonListener {
    fun onBackPressed(): Boolean
}