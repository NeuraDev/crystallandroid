package ru.agladkov.appreciate.interfaces

/**
 * Created by neura on 11.03.18.
 */
interface TemplateHandler {
    fun setTemplate(content: String)
    fun getTemplate(): String
}