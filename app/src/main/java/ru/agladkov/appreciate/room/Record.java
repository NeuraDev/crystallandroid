package ru.agladkov.appreciate.room;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by agladkov on 17.01.18.
 */

@Entity(tableName = "RECORD_TABLE")
public class Record {
    @PrimaryKey(autoGenerate = true)
    private int uid;

    @ColumnInfo(name = "date_time")
    private String dateTime;

    @ColumnInfo(name = "title")
    private String title;

    public Record(String dateTime, String title) {
        this.dateTime = dateTime;
        this.title = title;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
