package ru.agladkov.appreciate.room

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query

/**
 * Created by neura on 11.03.18.
 */
@Dao
interface TemplateDao {
    @Query("Select * FROM ${RoomContract.TemplateTable}")
    fun getAllTemplates(): List<TemplateModel>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addTemplate(templateModel: TemplateModel)

    @Query("DELETE FROM ${RoomContract.TemplateTable} WHERE id = :id")
    fun deleteById(id: Int)
}