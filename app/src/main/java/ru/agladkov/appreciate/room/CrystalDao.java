package ru.agladkov.appreciate.room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by agladkov on 17.01.18.
 */

@Dao
public interface CrystalDao {
    @Query("Select * FROM CRYSTAL_TABLE")
    List<Crystal> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addCrystal(Crystal crystal);
}
