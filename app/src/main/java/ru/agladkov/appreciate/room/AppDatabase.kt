package ru.agladkov.appreciate.room

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

/**
 * Created by agladkov on 17.01.18.
 */

@Database(entities = [(Record::class), (TemplateModel::class), (Crystal::class), (Configuration::class)], version = 5)
abstract class AppDatabase : RoomDatabase() {
    abstract fun recordDao(): RecordDao
    abstract fun templateDao(): TemplateDao
    abstract fun crystalDao(): CrystalDao
    abstract fun configurationDao(): ConfigurationDao

    companion object {
        fun buildDataSource(context: Context): AppDatabase = Room.databaseBuilder(
                context.applicationContext, AppDatabase::class.java, RoomContract.DATABASE_APP)
                .fallbackToDestructiveMigration()
                .build()
    }
}
