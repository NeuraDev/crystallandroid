package ru.agladkov.appreciate.room

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query

/**
 * Created by neura on 17.03.18.
 */
@Dao
interface ConfigurationDao {

    @Query("Select * FROM ${RoomContract.ConfigurationTable}")
    fun getConfigurations(): List<Configuration>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertConfiguration(configuration: Configuration)
}