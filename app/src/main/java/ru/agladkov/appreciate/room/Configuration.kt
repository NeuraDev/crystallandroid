package ru.agladkov.appreciate.room

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by neura on 17.03.18.
 */
@Entity(tableName = RoomContract.ConfigurationTable)
data class Configuration(@PrimaryKey val id: Int, var isSound: Boolean, var isNotifications: Boolean,
                         var isVibration: Boolean)