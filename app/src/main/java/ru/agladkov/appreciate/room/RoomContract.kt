package ru.agladkov.appreciate.room

/**
 * Created by neura on 09.03.18.
 */
class RoomContract {

    companion object {
        val DATABASE_APP: String = "crystal-android.db"

        const val TemplateTable = "TEMPLATE_TABLE"
        const val CrystalTable = "CRYSTAL_TABLE"
        const val ConfigurationTable = "CONFIGURATION_TABLE"
    }
}