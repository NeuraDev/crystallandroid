package ru.agladkov.appreciate.room;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by agladkov on 29.01.18.
 */

@Entity(tableName = RoomContract.CrystalTable)
public class Crystal {

    @PrimaryKey(autoGenerate = true)
    private Long id;

    @ColumnInfo(name = "date_time")
    private String dateTime;

    public Crystal(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
