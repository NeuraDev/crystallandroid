package ru.agladkov.appreciate.room

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import java.io.Serializable

/**
 * Created by neura on 10.03.18.
 */
@Entity(tableName = RoomContract.TemplateTable)
data class TemplateModel(@PrimaryKey(autoGenerate = true) val id: Int,
                         @ColumnInfo(name = "template_title") var title: String): Parcelable, Serializable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(title)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TemplateModel> {
        override fun createFromParcel(parcel: Parcel): TemplateModel {
            return TemplateModel(parcel)
        }

        override fun newArray(size: Int): Array<TemplateModel?> {
            return arrayOfNulls(size)
        }
    }
}