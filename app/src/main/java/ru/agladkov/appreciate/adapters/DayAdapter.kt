package ru.agladkov.appreciate.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import ru.agladkov.appreciate.R
import ru.agladkov.appreciate.base.BaseAdapter
import ru.agladkov.appreciate.base.BaseViewHolder
import ru.agladkov.appreciate.models.DayModel
import ru.agladkov.appreciate.models.JournalModel

/**
 * Created by neura on 09.03.18.
 */
class DayAdapter : BaseAdapter<DayModel>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<DayModel> {
        return DayAdapter.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.cell_day, parent, false))
    }

    class ViewHolder(itemView: View) : BaseViewHolder<DayModel>(itemView = itemView) {
        private val txtContent = itemView.findViewById<View>(R.id.txtDayContent) as TextView
        private val txtSectionTitle = itemView.findViewById<View>(R.id.txtDaySectionTitle) as TextView

        @SuppressLint("SetTextI18n")
        override fun bind(model: DayModel) {
            txtContent.text = model.content
            txtSectionTitle.text = model.sectionHeader

            if (model.sectionHeader == "") {
                txtSectionTitle.visibility = View.GONE
            } else {
                txtSectionTitle.visibility = View.VISIBLE
            }
        }
    }
}