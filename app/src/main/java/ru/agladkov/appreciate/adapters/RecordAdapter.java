package ru.agladkov.appreciate.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.agladkov.appreciate.R;
import ru.agladkov.appreciate.room.Record;

/**
 * Created by agladkov on 22.01.18.
 */

public class RecordAdapter extends RecyclerView.Adapter<RecordAdapter.RecordHolder> {
    private List<Record> dataList = new ArrayList<>();

    public void setData(List<Record> recordList) {
        dataList.clear();
        dataList.addAll(recordList);
        notifyDataSetChanged();
    }

    @Override
    public RecordHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new RecordHolder(inflater.inflate(R.layout.cell_record, parent, false));
    }

    @Override
    public void onBindViewHolder(RecordHolder holder, int position) {
        holder.bind(dataList.get(position), (position == 0 || !dataList.get(position - 1).getDateTime()
                .equalsIgnoreCase(dataList.get(position).getDateTime())));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    static class RecordHolder extends RecyclerView.ViewHolder {
        private TextView mTxtHeader;
        private TextView mTxtTitle;

        RecordHolder(View itemView) {
            super(itemView);
            mTxtTitle = itemView.findViewById(R.id.txtRecordTitle);
            mTxtHeader = itemView.findViewById(R.id.txtRecordHeader);
        }

        void bind(Record record, boolean hasHeader) {
            mTxtHeader.setVisibility(hasHeader ? View.VISIBLE : View.GONE);
            mTxtHeader.setText(record.getDateTime());
            mTxtTitle.setText(record.getTitle());
        }
    }
}
