package ru.agladkov.appreciate.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import ru.agladkov.appreciate.R
import ru.agladkov.appreciate.base.BaseAdapter
import ru.agladkov.appreciate.base.BaseViewHolder
import ru.agladkov.appreciate.models.AboutModel
import ru.agladkov.appreciate.models.JournalModel

/**
 * Created by neura on 09.03.18.
 */
class JournalAdapter : BaseAdapter<JournalModel>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<JournalModel> {
        return JournalAdapter.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.cell_journal, parent, false))
    }

    class ViewHolder(itemView: View) : BaseViewHolder<JournalModel>(itemView = itemView) {
        private val txtTitle = itemView.findViewById<View>(R.id.txtJournalTitle) as TextView
        private val txtCount = itemView.findViewById<View>(R.id.txtJournalCount) as TextView
        private val llHeader = itemView.findViewById<View>(R.id.llJournalHeader) as LinearLayout
        private val txtSectionTitle = itemView.findViewById<View>(R.id.txtJournalSectionTitle) as TextView

        @SuppressLint("SetTextI18n")
        override fun bind(model: JournalModel) {
            txtTitle.text = model.title
            txtCount.text = "${model.recordsCount}"
            txtSectionTitle.text = model.sectionHeader

            if (model.sectionHeader == "") {
                llHeader.visibility = View.GONE
            } else {
                llHeader.visibility = View.VISIBLE
            }
        }
    }
}