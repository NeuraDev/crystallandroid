package ru.agladkov.appreciate.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ru.agladkov.appreciate.R
import ru.agladkov.appreciate.base.BaseAdapter
import ru.agladkov.appreciate.base.BaseViewHolder
import ru.agladkov.appreciate.room.TemplateModel

/**
 * Created by neura on 09.03.18.
 */
class TemplateAdapter : BaseAdapter<TemplateModel>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<TemplateModel> {
        return TemplateAdapter.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.cell_template, parent, false))
    }

    class ViewHolder(itemView: View) : BaseViewHolder<TemplateModel>(itemView = itemView) {
        private val txtTitle = itemView.findViewById<View>(R.id.txtTemplateTitle) as TextView

        @SuppressLint("SetTextI18n")
        override fun bind(model: TemplateModel) {
            txtTitle.text = model.title
        }
    }
}