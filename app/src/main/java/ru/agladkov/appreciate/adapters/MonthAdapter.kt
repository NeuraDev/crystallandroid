package ru.agladkov.appreciate.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import ru.agladkov.appreciate.R
import ru.agladkov.appreciate.base.BaseAdapter
import ru.agladkov.appreciate.base.BaseViewHolder
import ru.agladkov.appreciate.models.JournalModel
import ru.agladkov.appreciate.models.MonthModel

/**
 * Created by neura on 09.03.18.
 */
class MonthAdapter : BaseAdapter<MonthModel>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<MonthModel> {
        return MonthAdapter.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.cell_month, parent, false))
    }

    class ViewHolder(itemView: View) : BaseViewHolder<MonthModel>(itemView = itemView) {
        private val txtDayNumber = itemView.findViewById<View>(R.id.txtMonthDayNumber) as TextView
        private val txtMonthName = itemView.findViewById<View>(R.id.txtMonthName) as TextView
        private val txtPercent = itemView.findViewById<View>(R.id.txtMonthPercent) as TextView
        private val txtCount = itemView.findViewById<View>(R.id.txtMonthCount) as TextView

        @SuppressLint("SetTextI18n")
        override fun bind(model: MonthModel) {
            txtDayNumber.text = "${model.dayNumber}"
            txtMonthName.text = model.monthName
            txtPercent.text = "${model.percent}%"
            txtCount.text = "${model.recordsCount}"
        }
    }
}