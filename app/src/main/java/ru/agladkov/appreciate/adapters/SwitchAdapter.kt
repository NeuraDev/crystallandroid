package ru.agladkov.appreciate.adapters

import android.annotation.SuppressLint
import android.support.v7.widget.SwitchCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.Switch
import android.widget.TextView
import ru.agladkov.appreciate.R
import ru.agladkov.appreciate.base.BaseAdapter
import ru.agladkov.appreciate.base.BaseViewHolder
import ru.agladkov.appreciate.models.AboutModel
import ru.agladkov.appreciate.models.SwitchModel

/**
 * Created by neura on 09.03.18.
 */
class SwitchAdapter : BaseAdapter<SwitchModel>() {
    private val TAG: String = SwitchAdapter::class.java.simpleName
    private var listener: SwitchListener? = null

    interface SwitchListener {
        fun onSwitch(id: Int, isOn: Boolean)
    }

    fun attachListener(listener: SwitchListener) {
        this.listener = listener
    }

    fun detachListener() {
        this.listener = null
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<SwitchModel> {
        return SwitchAdapter.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.cell_switch, parent, false))
    }

    override fun onBindViewHolder(holder: BaseViewHolder<SwitchModel>, position: Int) {
        super.onBindViewHolder(holder, position)
        if (holder is ViewHolder) {
            holder.switcher.setOnCheckedChangeListener { _, p1 ->
                listener?.onSwitch(id = mDataList[position].id, isOn = p1)
            }
        }
    }

    class ViewHolder(itemView: View) : BaseViewHolder<SwitchModel>(itemView = itemView) {
        private val TAG: String = SwitchAdapter::class.java.simpleName
        private val txtTitle = itemView.findViewById<View>(R.id.txtSwitchTitle) as TextView
        val switcher = itemView.findViewById<View>(R.id.swSwitch) as SwitchCompat

        @SuppressLint("SetTextI18n")
        override fun bind(model: SwitchModel) {
            txtTitle.text = model.title
            switcher.isChecked = model.isOn
        }
    }
}