package ru.agladkov.appreciate.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ru.agladkov.appreciate.R
import ru.agladkov.appreciate.base.BaseAdapter
import ru.agladkov.appreciate.base.BaseViewHolder
import ru.agladkov.appreciate.models.AboutModel

/**
 * Created by neura on 09.03.18.
 */
class AboutAdapter: BaseAdapter<AboutModel>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<AboutModel> {
        return AboutAdapter.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.cell_about, parent, false))
    }

    class ViewHolder(itemView: View) : BaseViewHolder<AboutModel>(itemView = itemView) {
        private val txtTitle = itemView.findViewById<View>(R.id.txtAboutTitle) as TextView

        @SuppressLint("SetTextI18n")
        override fun bind(model: AboutModel) {
            txtTitle.text = model.title
        }
    }
}