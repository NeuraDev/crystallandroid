package ru.agladkov.appreciate.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ru.agladkov.appreciate.R
import ru.agladkov.appreciate.base.BaseAdapter
import ru.agladkov.appreciate.base.BaseViewHolder
import ru.agladkov.appreciate.models.AboutModel
import ru.agladkov.appreciate.models.DonateModel

/**
 * Created by neura on 09.03.18.
 */
class DonateAdapter : BaseAdapter<DonateModel>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<DonateModel> {
        return DonateAdapter.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.cell_donate, parent, false))
    }

    class ViewHolder(itemView: View) : BaseViewHolder<DonateModel>(itemView = itemView) {
        private val txtTitle = itemView.findViewById<View>(R.id.txtDonateTitle) as TextView
        private val txtCost = itemView.findViewById<View>(R.id.txtDonateCost) as TextView

        @SuppressLint("SetTextI18n")
        override fun bind(model: DonateModel) {
            txtTitle.text = model.title
            txtCost.text = "${model.cost} ${model.currency}"
        }
    }
}