package ru.agladkov.appreciate.base

import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * Created by neura on 09.03.18.
 */
abstract class BaseViewHolder<T>(itemView: View): RecyclerView.ViewHolder(itemView) {
    abstract fun bind(model: T)
}