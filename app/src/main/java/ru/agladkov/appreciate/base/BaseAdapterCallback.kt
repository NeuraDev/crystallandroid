package ru.agladkov.appreciate.base

import android.view.View

/**
 * Created by neura on 09.03.18.
 */
interface BaseAdapterCallback<T> {
    fun onItemClick(model: T, view: View)
    fun onLongClick(model: T, view: View): Boolean
}