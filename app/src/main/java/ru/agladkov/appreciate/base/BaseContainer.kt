package ru.agladkov.appreciate.base

import android.os.Bundle
import com.arellomobile.mvp.MvpAppCompatFragment
import ru.agladkov.appreciate.R
import ru.agladkov.appreciate.di.App
import ru.agladkov.appreciate.di.App.Companion.appComponent
import ru.agladkov.appreciate.helpers.Keys
import ru.agladkov.appreciate.interfaces.BackButtonListener
import ru.agladkov.appreciate.interfaces.RedrawHandler
import ru.agladkov.appreciate.interfaces.RouterProvider
import ru.agladkov.appreciate.navigation.LocalCiceroneHolder
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Created by neura on 09.03.18.
 */
abstract class BaseContainer : MvpAppCompatFragment(), RouterProvider, BackButtonListener,
        RedrawHandler {
    private val TAG: String = BaseContainer::class.java.simpleName
    private val containerName = arguments?.getString(Keys.Name.value)
    var needsToRefresh = false

    @Inject
    lateinit var ciceroneHolder: LocalCiceroneHolder

    abstract fun doubleTap()
    abstract fun updateContainer()

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(fragment = this)
        super.onCreate(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        getCicerone().navigatorHolder.setNavigator(getNavigator())
    }

    override fun onPause() {
        getCicerone().navigatorHolder.removeNavigator()
        super.onPause()
    }

    protected fun getCicerone(): Cicerone<Router> {
        return ciceroneHolder.getCicerone(containerName.toString())
    }

    open fun getNavigator(): Navigator? {
        throw NotImplementedError()
    }

    // Force redraw fragment
    override fun refreshCurrentFragment() {
        if (needsToRefresh) {
            needsToRefresh = false
            val fragment = childFragmentManager.findFragmentById(R.id.container)
            if (fragment != null) {
                childFragmentManager
                        .beginTransaction()
                        .detach(fragment)
                        .attach(fragment)
                        .commit()
            }
        }
    }

    // MARK: - BackButtonListener implementation
    override fun onBackPressed(): Boolean {
        val fragment = childFragmentManager.findFragmentById(R.id.container)
        return if (fragment != null && fragment is BackButtonListener && fragment.onBackPressed()) {
            true
        } else {
            activity?.let { (it as RouterProvider).getRouter().exit() }
            true
        }
    }

    // MARK: - RouterProvider
    override fun getRouter(): Router {
        return getCicerone().router
    }
}