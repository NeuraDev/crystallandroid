package ru.agladkov.appreciate.fragments

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_template_add.*
import ru.agladkov.appreciate.R
import ru.agladkov.appreciate.adapters.TemplateAdapter
import ru.agladkov.appreciate.base.BaseChildFragment
import ru.agladkov.appreciate.di.App
import ru.agladkov.appreciate.helpers.ListConfig
import ru.agladkov.appreciate.helpers.ScreenKeys
import ru.agladkov.appreciate.interfaces.KeyboardHandler
import ru.agladkov.appreciate.interfaces.RouterProvider
import ru.agladkov.appreciate.presenters.TemplatePresenter
import ru.agladkov.appreciate.room.AppDatabase
import ru.agladkov.appreciate.room.TemplateModel
import ru.agladkov.appreciate.views.TemplateView
import java.util.*
import javax.inject.Inject

/**
 * Created by neura on 09.03.18.
 */

class TemplateAddFragment : BaseChildFragment() {
    private val TAG: String = TemplateAddFragment::class.java.simpleName

    companion object {
        fun getNewInstance(): TemplateAddFragment {
            return TemplateAddFragment()
        }
    }

    @Inject
    lateinit var appDatabase: AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(fragment = this@TemplateAddFragment)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_template_add, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mCallback?.updateActionBar(isBack = false, isHistory = false, title = getString(R.string.title_add_comment))
        val handler = Handler()
        llTemplateAdd.setOnTouchListener { _, _ ->
            (activity as? KeyboardHandler)?.hideKeyboard()
            true
        }
        btnTemplateAdd.setOnClickListener {
            Thread({
                if (textTemplateAdd.text.toString() != "") {
                    Thread({
                        appDatabase.templateDao().addTemplate(templateModel = TemplateModel(id = 0,
                                title = textTemplateAdd.text.toString()))
                        handler.post {
                            (parentFragment as? RouterProvider)?.getRouter()?.exit()
                        }
                    }).start()
                }
            }).start()
        }
    }
}