package ru.agladkov.appreciate.fragments

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import kotlinx.android.synthetic.main.fragment_add_comment.*
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import ru.agladkov.appreciate.R
import ru.agladkov.appreciate.adapters.DayAdapter
import ru.agladkov.appreciate.base.BaseAdapterCallback
import ru.agladkov.appreciate.base.BaseChildFragment
import ru.agladkov.appreciate.di.App
import ru.agladkov.appreciate.helpers.*
import ru.agladkov.appreciate.interfaces.KeyboardHandler
import ru.agladkov.appreciate.interfaces.RouterProvider
import ru.agladkov.appreciate.models.DayModel
import ru.agladkov.appreciate.room.AppDatabase
import ru.agladkov.appreciate.room.Record
import java.util.*
import javax.inject.Inject
import android.R.attr.label
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Context.CLIPBOARD_SERVICE
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_template_add.*
import ru.agladkov.appreciate.room.TemplateModel


/**
 * Created by neura on 09.03.18.
 */

class AddCommentFragment : BaseChildFragment() {
    private val mAdapter = DayAdapter()
    private val items: MutableList<DayModel> = LinkedList()
    private val TAG: String = AddCommentFragment::class.java.simpleName

    @Inject
    lateinit var appDatabase: AppDatabase

    companion object {
        fun getNewInstance(): AddCommentFragment {
            return AddCommentFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(fragment = this@AddCommentFragment)
        super.onCreate(savedInstanceState)
        mAdapter.attachCallback(object: BaseAdapterCallback<DayModel> {
            override fun onItemClick(model: DayModel, view: View) {
                (activity as? KeyboardHandler)?.hideKeyboard()
            }

            override fun onLongClick(model: DayModel, view: View): Boolean {
                context?.let { ctx ->
                    val dialog = MaterialDialog.Builder(ctx)
                            .customView(R.layout.dialog_add, false)
                            .build()

                    val btnCopy: LinearLayout = dialog.view.findViewById(R.id.btnCopy)
                    val btnTemplate: LinearLayout = dialog.view.findViewById(R.id.btnTemplate)

                    btnCopy.setOnClickListener {
                        val clipboard = activity?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                        val clip = ClipData.newPlainText(TAG, model.content)
                        clipboard.primaryClip = clip
                        Toast.makeText(ctx, getString(R.string.dialog_add_copied), Toast.LENGTH_SHORT).show()
                        dialog.dismiss()
                    }

                    btnTemplate.setOnClickListener {
                        val handler = Handler()
                        Thread({
                            appDatabase.templateDao().addTemplate(templateModel = TemplateModel(id = 0,
                                    title = model.content))

                            handler.post {
                                Toast.makeText(ctx, getString(R.string.dialog_add_templated), Toast.LENGTH_SHORT).show()
                                dialog.dismiss()
                            }
                        }).start()
                    }

                    dialog.show()
                }

                return true
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_add_comment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mCallback?.updateActionBar(isBack = false, isHistory = false, title = getString(R.string.title_add_comment))
        recyclerComment.setOnTouchListener { _, _ ->
            (activity as? KeyboardHandler)?.hideKeyboard()
            true
        }

        val listConfig = context?.let {
            ListConfig.Builder(mAdapter)
                    .setLayoutManagerProvider(ListConfig.ReversedLinearLayoutManagerProvider())
                    .setHasFixedSize(isFixedSize = true)
                    .setHasNestedScroll(isNestedScroll = true)
                    .build(context = it)
        }
        context?.let { listConfig?.applyConfig(context = it, recyclerView = recyclerComment) }

        imgAddCommentTemplate.setOnClickListener {
            (parentFragment as? RouterProvider)?.getRouter()?.navigateTo(ScreenKeys.Template.value)
        }

        imgAddCommentSend.setOnClickListener {
            if (textAddComment.text.toString().trim().isNotEmpty()) {
                if (mAdapter.itemCount < crystalsCount) {
                    val handler = Handler()
                    Thread({
                        appDatabase.recordDao().addRecord(Record(DateTime().toString("dd.MM.yyyy HH:mm:ss"),
                                textAddComment.text.toString()))
                        handler.post {
                            textAddComment.setText("")
                            (activity as? KeyboardHandler)?.hideKeyboard()
                            populateItems()
                        }
                    }).start()
                } else {
                    Toast.makeText(context, getString(R.string.not_enough_crystals), Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(context, getString(R.string.text_is_empty), Toast.LENGTH_SHORT).show()
            }

        }

        btnAddCommentBack.setOnClickListener {
            (parentFragment as? RouterProvider)?.getRouter()?.backTo(ScreenKeys.Crystal.value)
        }
        textAddComment.setText(AppData.template)
        populateItems()
    }

    private var crystalsCount = 0
    private fun populateItems() {
        val monthNumber = DateTime().monthOfYear
        val yearNumber = DateTime().year
        val dayNumber = DateTime().dayOfMonth
        val dateFormatter = DateTimeFormat.forPattern(DateHelper.pattern)

        val handler = Handler()
        Thread({
            val sortedCrystalls = appDatabase.crystalDao().all
                    .filter { dateFormatter.parseDateTime(it.dateTime).year == yearNumber }
                    .filter { dateFormatter.parseDateTime(it.dateTime).monthOfYear == monthNumber }
                    .filter { dateFormatter.parseDateTime(it.dateTime).dayOfMonth == dayNumber }
            this.crystalsCount = sortedCrystalls.size

            val sortedRecords = appDatabase.recordDao().all
                    .filter { dateFormatter.parseDateTime(it.dateTime).year == yearNumber }
                    .filter { dateFormatter.parseDateTime(it.dateTime).monthOfYear == monthNumber }
                    .filter { dateFormatter.parseDateTime(it.dateTime).dayOfMonth == dayNumber }
                    .sortedWith(compareBy({ dateFormatter.parseDateTime(it.dateTime) })).reversed()

            val items: MutableList<DayModel> = LinkedList()
            sortedRecords.forEach({
                items.add(DayModel(content = it.title, sectionHeader = ""))
            })

            handler.post {
                if (mAdapter.hasItems) {
                    mAdapter.updateItems(itemsList = items)
                } else {
                    mAdapter.setList(dataList = items)
                }
            }
        }).start()

    }
}