package ru.agladkov.appreciate.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import kotlinx.android.synthetic.main.fragment_day.*
import kotlinx.android.synthetic.main.fragment_journal.*
import kotlinx.android.synthetic.main.fragment_month.*
import org.joda.time.format.DateTimeFormat
import ru.agladkov.appreciate.R
import ru.agladkov.appreciate.adapters.DayAdapter
import ru.agladkov.appreciate.adapters.MonthAdapter
import ru.agladkov.appreciate.base.BaseAdapterCallback
import ru.agladkov.appreciate.base.BaseChildFragment
import ru.agladkov.appreciate.core.ScreenHelper
import ru.agladkov.appreciate.di.App
import ru.agladkov.appreciate.helpers.*
import ru.agladkov.appreciate.interfaces.RouterProvider
import ru.agladkov.appreciate.models.DayModel
import ru.agladkov.appreciate.models.MonthModel
import ru.agladkov.appreciate.room.AppDatabase
import ru.agladkov.appreciate.room.TemplateModel
import java.util.*
import javax.inject.Inject

/**
 * Created by neura on 09.03.18.
 */

class DayFragment : BaseChildFragment() {
    private val TAG: String = DayFragment::class.java.simpleName
    private val mAdapter = DayAdapter()

    @Inject
    lateinit var appDatabase: AppDatabase

    companion object {
        fun getNewInstance(args: Bundle?): DayFragment {
            val fragment = DayFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(fragment = this@DayFragment)
        super.onCreate(savedInstanceState)
        mAdapter.attachCallback(object: BaseAdapterCallback<DayModel> {
            override fun onItemClick(model: DayModel, view: View) {}
            override fun onLongClick(model: DayModel, view: View): Boolean {
                context?.let { ctx ->
                    val dialog = MaterialDialog.Builder(ctx)
                            .customView(R.layout.dialog_add, false)
                            .build()

                    val btnCopy: LinearLayout = dialog.view.findViewById(R.id.btnCopy)
                    val btnTemplate: LinearLayout = dialog.view.findViewById(R.id.btnTemplate)

                    btnCopy.setOnClickListener {
                        val clipboard = activity?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                        val clip = ClipData.newPlainText(TAG, model.content)
                        clipboard.primaryClip = clip
                        Toast.makeText(ctx, getString(R.string.dialog_add_copied), Toast.LENGTH_SHORT).show()
                        dialog.dismiss()
                    }

                    btnTemplate.setOnClickListener {
                        val handler = Handler()
                        Thread({
                            appDatabase.templateDao().addTemplate(templateModel = TemplateModel(id = 0,
                                    title = model.content))

                            handler.post {
                                Toast.makeText(ctx, getString(R.string.dialog_add_templated), Toast.LENGTH_SHORT).show()
                                dialog.dismiss()
                            }
                        }).start()
                    }

                    dialog.show()
                }

                return true
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_day, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mCallback?.updateActionBar(isBack = false, isHistory = false, title = getString(R.string.title_journal))

        val listConfig = context?.let {
            ListConfig.Builder(mAdapter)
                    .setLayoutManagerProvider(ListConfig.ReversedLinearLayoutManagerProvider())
                    .setHasFixedSize(isFixedSize = true)
                    .setHasNestedScroll(isNestedScroll = true)
                    .build(context = it)
        }
        context?.let { listConfig?.applyConfig(context = it, recyclerView = recyclerDay) }

        btnDayBack.setOnClickListener {
            (parentFragment as? RouterProvider)?.getRouter()?.exit()
        }

        btnDayShare.setOnClickListener {
            activity?.let {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (it.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                            || it.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(it, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE), 1)
                        return@setOnClickListener
                    }
                }

                Share.share(activity = it)
            }
        }

        btnDayPdf.setOnClickListener {
            activity?.let {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (it.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                            || it.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(it, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE), 1)
                        return@setOnClickListener
                    }
                }

                val pdfHelper = PdfHelper(recyclerDay, ScreenHelper.getScreen(it).x.toLong(),
                        ScreenHelper.getScreen(it).y.toLong())
                val file = pdfHelper.saveImageToPDF()

                val sharingIntent = Intent(Intent.ACTION_SEND)
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.app_name))
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.app_name))
                sharingIntent.putExtra(android.content.Intent.EXTRA_STREAM, Uri.parse("file://${file.absolutePath}"))
                Log.e(TAG, file.absolutePath)
                sharingIntent.type = "application/pdf"
                it.startActivity(Intent.createChooser(sharingIntent, it.resources?.getString(R.string.share_using)))
            }
        }

        populateItems(args = arguments)
    }

    @SuppressLint("SetTextI18n")
    private fun populateItems(args: Bundle?) {
        val monthNumber = args?.getInt(Keys.MonthNumber.value) ?: 0
        val yearNumber = args?.getInt(Keys.YearNumber.value) ?: 0
        val dayNumber = args?.getInt(Keys.DayNumber.value) ?: 0
        val dateFormatter = DateTimeFormat.forPattern(DateHelper.pattern)

        if (monthNumber == 0 || yearNumber == 0 || dayNumber == 0) {
            (parentFragment as? RouterProvider)?.getRouter()?.exit()
        } else {
            txtDayTitle.text = "$dayNumber ${DateHelper.monthForNumber(context = context, monthNumber = monthNumber)}"

            val handler = Handler()
            Thread({
                val sortedRecords = appDatabase.recordDao().all
                        .filter { dateFormatter.parseDateTime(it.dateTime).year == yearNumber }
                        .filter { dateFormatter.parseDateTime(it.dateTime).monthOfYear == monthNumber }
                        .filter { dateFormatter.parseDateTime(it.dateTime).dayOfMonth == dayNumber }
                        .sortedWith(compareBy({ dateFormatter.parseDateTime(it.dateTime) })).reversed()

                val items: MutableList<DayModel> = LinkedList()
                sortedRecords.forEach({
                    items.add(DayModel(content = it.title, sectionHeader = ""))
                })

                handler.post {
                    if (mAdapter.hasItems) {
                        mAdapter.updateItems(itemsList = items)
                    } else {
                        mAdapter.setList(dataList = items)
                    }
                }
            }).start()
        }
    }
}