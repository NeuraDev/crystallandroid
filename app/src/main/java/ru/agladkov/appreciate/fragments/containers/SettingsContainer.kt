package ru.agladkov.appreciate.fragments.containers

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ru.agladkov.appreciate.R
import ru.agladkov.appreciate.base.BaseContainer
import ru.agladkov.appreciate.fragments.*
import ru.agladkov.appreciate.helpers.Keys
import ru.agladkov.appreciate.helpers.ScreenKeys
import ru.agladkov.appreciate.interfaces.FragmentView
import ru.agladkov.appreciate.interfaces.RouterProvider
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command
import ru.terrakok.cicerone.commands.Forward
import ru.terrakok.cicerone.commands.Replace

/**
 * Created by neura on 09.03.18.
 */
class SettingsContainer: BaseContainer() {

    companion object {
        val TAG = SettingsContainer::class.java.simpleName

        fun getNewInstance(name: String): SettingsContainer {
            val fragment = SettingsContainer()
            val args = Bundle()
            args.putString(Keys.Name.value, name)

            fragment.arguments = args
            return fragment
        }
    }

    override fun doubleTap() {
        getCicerone().router.backTo(ScreenKeys.More.value)
    }

    override fun updateContainer() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.container, container, false)
    }

    private var mCallback: FragmentView? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val fragment = childFragmentManager.findFragmentById(R.id.container)
        if (fragment == null) {
            getCicerone().router.replaceScreen(ScreenKeys.More.value, 0)
            mCallback?.updateActionBar(isBack = false, title = getString(R.string.title_crystal),
                    isHistory = true)
        } else {
            when (fragment.javaClass.simpleName) {
                CrystalFragment::class.java.simpleName -> mCallback?.updateActionBar(isBack = false,
                        title = getString(R.string.title_crystal), isHistory = true)
            }
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        context?.let {
            if (it is FragmentView)
                mCallback = it
        }
    }

    override fun onDetach() {
        super.onDetach()
        mCallback = null
    }

    private var navigator: Navigator? = null
    override fun getNavigator(): Navigator {
        return if (navigator == null) {
            navigator = object: SupportAppNavigator(activity, childFragmentManager, R.id.container) {
                override fun createActivityIntent(context: Context?, screenKey: String?, data: Any?): Intent? {
                    return when (screenKey) {
                        ScreenKeys.Commend.value -> {
                            val shareBody = getString(R.string.commend_text)
                            val sharingIntent = Intent(android.content.Intent.ACTION_SEND)
                            sharingIntent.type = "text/plain"
                            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, R.string.app_name)
                            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody)
                            Intent.createChooser(sharingIntent, resources.getString(R.string.share_using))
                        }
                        ScreenKeys.Rate.value -> Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=ru.agladkov.appreciate"))
                        else -> null
                    }
                }

                override fun applyCommand(command: Command) {
                    super.applyCommand(command)
                    if (command is Forward) {
                        when (command.screenKey) {
                        // There is some animation for Intent operations by screenKey
                        // See ScreenKeys::class
                        }
                    }
                }

                override fun createFragment(screenKey: String?, data: Any?): Fragment? {
                    return when (screenKey) {
                        ScreenKeys.More.value -> MoreFragment.getNewInstance()
                        ScreenKeys.Donate.value -> DonateFragment.getNewInstance()
                        ScreenKeys.About.value -> AboutFragment.getNewInstance()
                        ScreenKeys.AboutDetail.value -> {
                            data?.let { AboutDetailFragment.getNewInstance(data = it as Bundle) }
                        }
                        ScreenKeys.Notifications.value -> NotificationsFragment.getNewInstance()
                        else -> null
                    }
                }

                override fun exit() {
                    super.exit()
                    activity?.let { (it as RouterProvider).getRouter().exit() }
                }

                override fun setupFragmentTransactionAnimation(command: Command, currentFragment: Fragment?,
                                                               nextFragment: Fragment, fragmentTransaction: FragmentTransaction) {
                    super.setupFragmentTransactionAnimation(command, currentFragment, nextFragment, fragmentTransaction)
                    if (command is Forward && nextFragment !is CrystalFragment) {
                        fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right,
                                R.anim.slide_in_right, R.anim.slide_out_left)
                    }

                    if (command is Replace && nextFragment is NotificationsFragment && currentFragment is MoreFragment) {
                        fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right,
                                R.anim.slide_in_right, R.anim.slide_out_left)
                    }

                    if (command is Replace && nextFragment is MoreFragment && currentFragment is NotificationsFragment) {
                        fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
                                R.anim.slide_in_left, R.anim.slide_out_right)
                    }

                    Log.e(TAG, "command ${command is Replace}, next $nextFragment, current $currentFragment")
                }
            }

            navigator as SupportAppNavigator
        } else {
            navigator!!
        }
    }
}