package ru.agladkov.appreciate.fragments

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_template_edit.*
import ru.agladkov.appreciate.R
import ru.agladkov.appreciate.base.BaseChildFragment
import ru.agladkov.appreciate.base.BaseContainer
import ru.agladkov.appreciate.di.App
import ru.agladkov.appreciate.helpers.Keys
import ru.agladkov.appreciate.helpers.ScreenKeys
import ru.agladkov.appreciate.interfaces.KeyboardHandler
import ru.agladkov.appreciate.interfaces.RouterProvider
import ru.agladkov.appreciate.room.AppDatabase
import ru.agladkov.appreciate.room.TemplateModel
import javax.inject.Inject

/**
 * Created by neura on 09.03.18.
 */

class TemplateEditFragment : BaseChildFragment() {
    private val TAG: String = TemplateEditFragment::class.java.simpleName

    companion object {
        fun getNewInstance(model: TemplateModel): TemplateEditFragment {
            val fragment = TemplateEditFragment()
            val bundle = Bundle()
            bundle.putSerializable(Keys.Template.value, model)
            fragment.arguments = bundle
            return fragment
        }
    }

    @Inject
    lateinit var appDatabase: AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(fragment = this@TemplateEditFragment)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_template_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        llTemplateEdit.setOnTouchListener { _, _ ->
            (activity as? KeyboardHandler)?.hideKeyboard()
            true
        }

        mCallback?.updateActionBar(isBack = false, isHistory = false, title = getString(R.string.title_add_comment))
        val model = arguments?.get(Keys.Template.value) as? TemplateModel
        textTemplateEdit.setText(model?.title ?: "")

        val handler = Handler()
        btnEditBack.setOnClickListener {
            (parentFragment as? RouterProvider)?.getRouter()?.backTo(ScreenKeys.TemplateEditDelete.value)
        }

        btnTemplateDelete.setOnClickListener {
            Thread({
                appDatabase.templateDao().deleteById(id = model?.id ?: -1)
                handler.post {
                    (parentFragment as? RouterProvider)?.getRouter()?.backTo(ScreenKeys.Template.value)
                }
            }).start()
        }

        btnTemplateEdit.setOnClickListener {
            if (textTemplateEdit.text.toString() != "") {
                Thread({
                    model?.let {
                        it.title = textTemplateEdit.text.toString()
                        appDatabase.templateDao().addTemplate(templateModel = it)
                        handler.post {
                            (parentFragment as? RouterProvider)?.getRouter()?.backTo(ScreenKeys.Template.value)
                        }
                    }
                }).start()
            }
        }
    }
}