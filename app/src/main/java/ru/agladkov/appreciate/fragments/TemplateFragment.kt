package ru.agladkov.appreciate.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_template.*
import ru.agladkov.appreciate.R
import ru.agladkov.appreciate.adapters.TemplateAdapter
import ru.agladkov.appreciate.base.BaseAdapterCallback
import ru.agladkov.appreciate.base.BaseChildFragment
import ru.agladkov.appreciate.base.BaseContainer
import ru.agladkov.appreciate.di.App
import ru.agladkov.appreciate.helpers.AppData
import ru.agladkov.appreciate.helpers.ListConfig
import ru.agladkov.appreciate.helpers.ScreenKeys
import ru.agladkov.appreciate.interfaces.RedrawHandler
import ru.agladkov.appreciate.interfaces.RouterProvider
import ru.agladkov.appreciate.interfaces.TemplateHandler
import ru.agladkov.appreciate.presenters.TemplatePresenter
import ru.agladkov.appreciate.room.AppDatabase
import ru.agladkov.appreciate.room.TemplateModel
import ru.agladkov.appreciate.views.TemplateView
import java.util.*
import javax.inject.Inject

/**
 * Created by neura on 09.03.18.
 */

class TemplateFragment : BaseChildFragment(), TemplateView {
    private val mAdapter = TemplateAdapter()
    private val TAG: String = TemplateFragment::class.java.simpleName

    companion object {
        fun getNewInstance(): TemplateFragment {
            return TemplateFragment()
        }
    }

    @Inject
    lateinit var appDatabase: AppDatabase

    @InjectPresenter
    lateinit var templatePresenter: TemplatePresenter

    @ProvidePresenter
    fun providePresenter(): TemplatePresenter {
        return TemplatePresenter(appDatabase = appDatabase)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(fragment = this@TemplateFragment)
        super.onCreate(savedInstanceState)
        mAdapter.attachCallback(object: BaseAdapterCallback<TemplateModel> {
            override fun onItemClick(model: TemplateModel, view: View) {
                AppData.template = model.title
                (parentFragment as? RouterProvider)?.getRouter()?.navigateTo(ScreenKeys.AddComment.value)
            }

            override fun onLongClick(model: TemplateModel, view: View): Boolean {
                return true
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (parentFragment as? RedrawHandler)?.refreshCurrentFragment()
        return inflater.inflate(R.layout.fragment_template, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mCallback?.updateActionBar(isBack = false, isHistory = false, title = getString(R.string.title_add_comment))
        val listConfig = context?.let {
            ListConfig.Builder(mAdapter)
                    .setHasFixedSize(isFixedSize = true)
                    .setHasNestedScroll(isNestedScroll = false)
                    .build(context = it)
        }
        context?.let { listConfig?.applyConfig(context = it, recyclerView = recyclerTemplate) }

        btnAdd.setOnClickListener {
            (parentFragment as? RouterProvider)?.getRouter()?.navigateTo(ScreenKeys.TemplateAdd.value)
        }

        btnTemplateBack.setOnClickListener {
            (parentFragment as? RouterProvider)?.getRouter()?.backTo(ScreenKeys.AddComment.value)
        }

        btnEditDelete.setOnClickListener {
            (parentFragment as? RouterProvider)?.getRouter()?.navigateTo(ScreenKeys.TemplateEditDelete.value)
        }

        templatePresenter.loadTemplates()
    }

    // MARK: - View implementation
    override fun setupNoItems() {
        recyclerTemplate.visibility = View.GONE
        btnEditDelete.visibility = View.GONE
        txtTemplateNoItems.visibility = View.VISIBLE
    }

    override fun setupItems(data: List<TemplateModel>) {
        txtTemplateNoItems.visibility = View.GONE
        recyclerTemplate.visibility = View.VISIBLE
        btnEditDelete.visibility = View.VISIBLE
        if (mAdapter.hasItems) {
            mAdapter.updateItems(itemsList =  data)
        } else {
            mAdapter.setList(dataList = data)
        }
    }
}