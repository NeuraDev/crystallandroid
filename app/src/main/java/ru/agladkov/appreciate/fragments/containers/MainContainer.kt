package ru.agladkov.appreciate.fragments.containers

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ru.agladkov.appreciate.R
import ru.agladkov.appreciate.base.BaseContainer
import ru.agladkov.appreciate.fragments.*
import ru.agladkov.appreciate.helpers.Keys
import ru.agladkov.appreciate.helpers.ScreenKeys
import ru.agladkov.appreciate.interfaces.FragmentView
import ru.agladkov.appreciate.interfaces.RouterProvider
import ru.agladkov.appreciate.interfaces.TemplateHandler
import ru.agladkov.appreciate.room.TemplateModel
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.SupportAppNavigator
import ru.terrakok.cicerone.commands.Back
import ru.terrakok.cicerone.commands.Command
import ru.terrakok.cicerone.commands.Forward
import ru.terrakok.cicerone.commands.Replace

/**
 * Created by neura on 09.03.18.
 */
class MainContainer: BaseContainer() {

    companion object {
        val TAG = MainContainer::class.java.simpleName

        fun getNewInstance(name: String): MainContainer {
            val fragment = MainContainer()
            val args = Bundle()
            args.putString(Keys.Name.value, name)

            fragment.arguments = args
            return fragment
        }
    }

    override fun doubleTap() {
        getCicerone().router.backTo(ScreenKeys.Crystal.value)
    }

    override fun updateContainer() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.container, container, false)
    }

    private var mCallback: FragmentView? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val fragment = childFragmentManager.findFragmentById(R.id.container)
        if (fragment == null) {
            getCicerone().router.replaceScreen(ScreenKeys.Crystal.value, 0)
            mCallback?.updateActionBar(isBack = false, title = getString(R.string.title_crystal),
                    isHistory = true)
        } else {
            when (fragment.javaClass.simpleName) {
                CrystalFragment::class.java.simpleName -> mCallback?.updateActionBar(isBack = false,
                        title = getString(R.string.title_crystal), isHistory = true)
            }
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        context?.let {
            if (it is FragmentView)
                mCallback = it
        }
    }

    override fun onDetach() {
        super.onDetach()
        mCallback = null
    }

    private var navigator: Navigator? = null
    override fun getNavigator(): Navigator {
        return if (navigator == null) {
//            navigator = Navigator({ commands ->
//                commands.forEach {
//                    when (it) {
//                        is Back -> activity?.let { (it as RouterProvider).getRouter().exit() }
//                        is Forward -> {
//                            Log.e(TAG, "it ${it.screenKey}")
//                        }
//                        is Replace -> {
//                            ch
//                        }
//                    }
//                }
//            })
            navigator = object: SupportAppNavigator(activity, childFragmentManager, R.id.container) {
                override fun createActivityIntent(context: Context?, screenKey: String?, data: Any?): Intent? {
                    return when (screenKey) {
                    // There is some screen keys for Intent operations
                        else -> null
                    }
                }

                override fun createFragment(screenKey: String?, data: Any?): Fragment? {
                    return when (screenKey) {
                        ScreenKeys.Crystal.value -> CrystalFragment.getNewInstance()
                        ScreenKeys.AddComment.value -> AddCommentFragment.getNewInstance()
                        ScreenKeys.Template.value -> TemplateFragment.getNewInstance()
                        ScreenKeys.TemplateAdd.value -> TemplateAddFragment.getNewInstance()
                        ScreenKeys.TemplateEditDelete.value -> TemplateEditDeleteFragment.getNewInstance()
                        ScreenKeys.TemplateEdit.value -> TemplateEditFragment.getNewInstance(model = (data as TemplateModel))
                        else -> null
                    }
                }

                override fun exit() {
                    super.exit()
                    activity?.let { (it as RouterProvider).getRouter().exit() }
                }

                override fun setupFragmentTransactionAnimation(command: Command, currentFragment: Fragment?,
                                                               nextFragment: Fragment, fragmentTransaction: FragmentTransaction) {
                    super.setupFragmentTransactionAnimation(command, currentFragment, nextFragment, fragmentTransaction)
                    if (command is Forward && nextFragment !is CrystalFragment) {
                        fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right,
                                R.anim.slide_in_right, R.anim.slide_out_left)
                    }
                }
            }

            navigator as SupportAppNavigator
//            navigator!!
        } else {
            navigator!!
        }
    }
}