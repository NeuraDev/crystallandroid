package ru.agladkov.appreciate.fragments

import android.Manifest
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_about.*
import kotlinx.android.synthetic.main.fragment_journal.*
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import ru.agladkov.appreciate.R
import ru.agladkov.appreciate.adapters.JournalAdapter
import ru.agladkov.appreciate.base.BaseAdapterCallback
import ru.agladkov.appreciate.base.BaseChildFragment
import ru.agladkov.appreciate.di.App
import ru.agladkov.appreciate.interfaces.RouterProvider
import ru.agladkov.appreciate.models.JournalModel
import ru.agladkov.appreciate.room.AppDatabase
import ru.agladkov.appreciate.room.Crystal
import ru.agladkov.appreciate.room.Record
import java.util.*
import javax.inject.Inject
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.support.v4.app.ActivityCompat
import kotlinx.android.synthetic.main.fragment_day.*
import ru.agladkov.appreciate.core.ScreenHelper
import ru.agladkov.appreciate.helpers.*


/**
 * Created by neura on 09.03.18.
 */

class JournalFragment : BaseChildFragment() {
    private val TAG: String = JournalFragment::class.java.simpleName
    private val mAdapter = JournalAdapter()

    companion object {
        fun getNewInstance(): JournalFragment {
            return JournalFragment()
        }
    }

    @Inject
    lateinit var appDatabase: AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(fragment = this@JournalFragment)
        super.onCreate(savedInstanceState)
        mAdapter.attachCallback(object : BaseAdapterCallback<JournalModel> {
            override fun onItemClick(model: JournalModel, view: View) {
                val bundle = Bundle()
                bundle.putInt(Keys.MonthNumber.value, model.month)
                bundle.putInt(Keys.YearNumber.value, model.year)
                (parentFragment as? RouterProvider)?.getRouter()?.navigateTo(ScreenKeys.Month.value,
                        bundle)
            }

            override fun onLongClick(model: JournalModel, view: View): Boolean {
                return true
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_journal, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mCallback?.updateActionBar(isBack = false, isHistory = false, title = getString(R.string.title_journal))

        val listConfig = context?.let {
            ListConfig.Builder(mAdapter)
                    .setHasFixedSize(isFixedSize = true)
                    .setHasNestedScroll(isNestedScroll = true)
                    .build(context = it)
        }
        context?.let { listConfig?.applyConfig(context = it, recyclerView = recyclerJournal) }

        btnJournalShare.setOnClickListener {
            activity?.let {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (it.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                            || it.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(it, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE), 1)
                        return@setOnClickListener
                    }
                }

                Share.share(activity = it)
            }
        }

        btnJournalPdf.setOnClickListener {
            activity?.let {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (it.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                            || it.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(it, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE), 1)
                        return@setOnClickListener
                    }
                }

                val pdfHelper = PdfHelper(recyclerJournal, ScreenHelper.getScreen(it).x.toLong(),
                        ScreenHelper.getScreen(it).y.toLong())
                val file = pdfHelper.saveImageToPDF()

                val sharingIntent = Intent(Intent.ACTION_SEND)
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.app_name))
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.app_name))
                sharingIntent.putExtra(android.content.Intent.EXTRA_STREAM, Uri.parse("file://${file.absolutePath}"))
                Log.e(TAG, file.absolutePath)
                sharingIntent.type = "application/pdf"
                it.startActivity(Intent.createChooser(sharingIntent, it.resources?.getString(R.string.share_using)))
            }
        }

        populateItems()
    }

    private fun populateItems() {
        val handler = Handler()
        Thread({
            val dateFormatter = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm:ss")
            val records = appDatabase.recordDao().all
            val sortedRecords = records.sortedWith(compareBy({ dateFormatter.parseDateTime(it.dateTime) })).reversed()
            val crystals = appDatabase.crystalDao().all
            val sortedCrystals = crystals.sortedWith(compareBy({ dateFormatter.parseDateTime(it.dateTime) })).reversed()

            val items: MutableList<JournalModel> = LinkedList()
            sortedRecords.forEach({ record ->
                if (sortedRecords.indexOf(record) == 0 || dateFormatter.parseDateTime(record.dateTime).year <
                        dateFormatter.parseDateTime(sortedRecords[sortedRecords.indexOf(record) - 1].dateTime).year) {
                    val recordDateTime = dateFormatter.parseDateTime(record.dateTime)
                    val monthMap = DateHelper.extractMonthCountForYear(recordDateTime.year, records)

                    monthMap.keys.forEach({ it ->
                        items.add(JournalModel(id = sortedRecords.indexOf(record) + it,
                                title = DateHelper.monthForNumber(context = context, monthNumber = it),
                                sectionHeader = "", recordsCount = monthMap[it] ?: 0,
                                year = dateFormatter.parseDateTime(record.dateTime).year,
                                month = dateFormatter.parseDateTime(record.dateTime).monthOfYear))
                    })
                }
            })

            val extractedYears = DateHelper.extractYearsForCrystals(sortedCrystals)
            extractedYears.keys.forEach({ year ->
                val extractedYearsItems = DateHelper.extractYearsForJournal(items)

                val crystalMonth = DateHelper.extractMonthCountForYearC(year, crystals)
                if (extractedYearsItems.keys.contains(year)) {
                    val extractedMonth = DateHelper.extractMonthCountForYearJournal(year, items)
                    crystalMonth.keys.forEach({ month ->
                        if (!extractedMonth.keys.contains(month)) {
                            items.add(JournalModel(id = 0,
                                    title = DateHelper.monthForNumber(context = context, monthNumber = month),
                                    sectionHeader = "",
                                    recordsCount = 0, year = year, month = month))
                        }
                    })
                } else {
                    crystalMonth.keys.forEach({ month ->
                        items.add(JournalModel(id = 0,
                                title = DateHelper.monthForNumber(context = context, monthNumber = month),
                                sectionHeader = "",
                                recordsCount = 0, year = year, month = month))
                    })
                }
            })

            val sortedItems = items.sortedWith(compareBy({ it.year }, { it.month })).reversed()
            sortedItems.forEach({ item ->
                val index = sortedItems.indexOf(item)
                when (index) {
                    0 -> sortedItems[0].sectionHeader = "${item.year}"
                    else -> {
                        if (sortedItems[index - 1].year > sortedItems[index].year) {
                            sortedItems[index].sectionHeader = "${item.year}"
                        }
                    }
                }

            })

            handler.post {
                if (mAdapter.hasItems) {
                    mAdapter.updateItems(itemsList = sortedItems)
                } else {
                    mAdapter.setList(dataList = sortedItems)
                }
            }
        }).start()
    }
}