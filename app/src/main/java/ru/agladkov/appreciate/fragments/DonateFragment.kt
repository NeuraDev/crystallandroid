package ru.agladkov.appreciate.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_about.*
import kotlinx.android.synthetic.main.fragment_donate.*
import ru.agladkov.appreciate.R
import ru.agladkov.appreciate.adapters.DonateAdapter
import ru.agladkov.appreciate.base.BaseChildFragment
import ru.agladkov.appreciate.helpers.BillingProvider
import ru.agladkov.appreciate.helpers.ListConfig
import ru.agladkov.appreciate.interfaces.RouterProvider
import ru.agladkov.appreciate.models.DonateModel
import java.util.*
import com.anjlab.android.iab.v3.BillingProcessor
import ru.agladkov.appreciate.base.BaseAdapterCallback
import kotlin.collections.ArrayList


/**
 * Created by neura on 09.03.18.
 */

class DonateFragment : BaseChildFragment() {
    private val mAdapter = DonateAdapter()
    private val TAG = DonateFragment::class.java.simpleName
    private val items: MutableList<DonateModel> = LinkedList()

    companion object {
        fun getNewInstance(): DonateFragment {
            return DonateFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        populateItems()
        mAdapter.setList(dataList = items)
        mAdapter.attachCallback(object: BaseAdapterCallback<DonateModel> {
            override fun onItemClick(model: DonateModel, view: View) {
                (activity as? BillingProvider)?.getBilling()?.purchase(activity, model.id)
            }

            override fun onLongClick(model: DonateModel, view: View): Boolean {
                return true
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_donate, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mCallback?.updateActionBar(isBack = false, isHistory = false, title = getString(R.string.title_donate))

        val isAvailable = BillingProcessor.isIabServiceAvailable(context)
        if (isAvailable) {
            val skuList = ArrayList<String>()
            val itemsList: MutableList<DonateModel> = LinkedList()
            skuList.add("coffee")
            skuList.add("fruits")
            skuList.add("lunch")
            val skuDetails = (activity as? BillingProvider)?.getBilling()?.getPurchaseListingDetails(skuList)
            skuDetails?.forEach {
                itemsList.add(DonateModel(id = it.productId, title = it.title, currency = it.currency,
                        cost = it.priceValue, description = it.description))
            }

            if (mAdapter.hasItems) {
                mAdapter.updateItems(itemsList = itemsList)
            } else {
                mAdapter.setList(dataList = itemsList)
            }
        }


        val listConfig = context?.let {
            ListConfig.Builder(mAdapter)
                    .setHasFixedSize(isFixedSize = true)
                    .setHasNestedScroll(isNestedScroll = false)
                    .build(context = it)
        }
        context?.let { listConfig?.applyConfig(context = it, recyclerView = recyclerDonate) }
        btnDonateBack.setOnClickListener { (parentFragment as? RouterProvider)?.getRouter()?.exit() }
    }

    private fun populateItems() {
//        items.add(DonateModel(id = 0, title = "Water", cost = "0.5", currency = "$"))
//        items.add(DonateModel(id = 1, title = "Fresh", cost = "2", currency = "$"))
//        items.add(DonateModel(id = 2, title = "Lunch", cost = "10", currency = "$"))
    }
}