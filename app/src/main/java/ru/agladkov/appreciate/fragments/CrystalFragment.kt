package ru.agladkov.appreciate.fragments

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_crystal.*
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import ru.agladkov.appreciate.R
import ru.agladkov.appreciate.base.BaseChildFragment
import ru.agladkov.appreciate.core.Finger
import ru.agladkov.appreciate.core.ScreenHelper
import ru.agladkov.appreciate.core.Zone
import ru.agladkov.appreciate.di.App
import ru.agladkov.appreciate.interfaces.MapInterface
import ru.agladkov.appreciate.interfaces.RouterProvider
import ru.agladkov.appreciate.room.AppDatabase
import ru.agladkov.appreciate.room.Crystal
import ru.agladkov.appreciate.room.Record
import javax.inject.Inject
import android.R.raw
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ObjectAnimator
import android.app.Dialog
import android.content.Context
import android.media.MediaPlayer
import android.os.*
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import com.afollestad.materialdialogs.MaterialDialog
import pl.droidsonroids.gif.GifDrawable
import ru.agladkov.appreciate.helpers.*
import ru.agladkov.appreciate.room.Configuration
import java.util.*


/**
 * Created by neura on 09.03.18.
 */

class CrystalFragment : BaseChildFragment() {
    private val TAG: String = CrystalFragment::class.java.simpleName

    companion object {
        fun getNewInstance(): CrystalFragment {
            return CrystalFragment()
        }
    }

    @Inject
    lateinit var appDatabase: AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(fragment = this@CrystalFragment)
        super.onCreate(savedInstanceState)
        Thread({
            val configs = appDatabase.configurationDao().getConfigurations().toMutableList()

            if (configs.isEmpty()) {
                val newConfig = Configuration(id = 0, isSound = true, isNotifications = true, isVibration = true)
                configs.add(0, newConfig)
                appDatabase.configurationDao().insertConfiguration(configuration = newConfig)
                AppData.appConfiguration = newConfig
            } else {
                AppData.appConfiguration = configs.first()
            }
        }).start()

        val timeOfDay = Calendar.getInstance()
        timeOfDay.set(Calendar.HOUR_OF_DAY, 0)
        timeOfDay.set(Calendar.MINUTE, 0)
        timeOfDay.set(Calendar.SECOND, 0)

        val handler = Handler()
        MyTaskExecutor(timeOfDay, Runnable {
            try {
                handler.post {
                    clearCrystals()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }, "daily-housekeeping").start()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_crystal, container, false)
    }

    var crystalCount = 0
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mCallback?.updateActionBar(isBack = false, isHistory = false, title = getString(R.string.title_journal))

        imgCrystal.setOnClickListener {
            (parentFragment as? RouterProvider)?.getRouter()?.navigateTo(ScreenKeys.AddComment.value)
        }

        val handler = Handler()
        Thread({
            val dateFormatter = DateTimeFormat.forPattern(DateHelper.pattern)
            val yearNumber = DateTime().year
            val monthNumber = DateTime().monthOfYear
            val dayNumber = DateTime().dayOfMonth

            val crystalsCount = appDatabase.crystalDao().all
                    .filter { dateFormatter.parseDateTime(it.dateTime).year == yearNumber }
                    .filter { dateFormatter.parseDateTime(it.dateTime).monthOfYear == monthNumber }
                    .filter { dateFormatter.parseDateTime(it.dateTime).dayOfMonth == dayNumber }
                    .count()

            if (crystalsCount >= 0) {
                handler.post {
                    crystalCount = crystalsCount

                    when (crystalCount) {
//                        in 0..6 -> Glide.with(this).load(R.drawable.crystal_1).into(GifDrawableImageViewTarget(imgCrystal, 0))
//                        in 7..13 -> Glide.with(this).load(R.drawable.crystal_2).into(GifDrawableImageViewTarget(imgCrystal, 0))
//                        in 14..20 -> Glide.with(this).load(R.drawable.crystal_3).into(GifDrawableImageViewTarget(imgCrystal, 0))
//                        else -> Glide.with(this).load(R.drawable.crystal_4).into(GifDrawableImageViewTarget(imgCrystal, 0))
                        in 0..6 -> imgCrystal.gifResource = R.drawable.crystal_1
                        in 7..13 -> imgCrystal.gifResource = R.drawable.crystal_2
                        in 14..20 -> imgCrystal.gifResource = R.drawable.crystal_3
                        else -> imgCrystal.gifResource = R.drawable.crystal_4
                    }

                    imgCrystal.pause()
                    txtCrystalCount.text = "$crystalsCount"
                }
            }
        }).start()

        // Here is the main trick
        context?.let {
            val width = ScreenHelper.getScreen(activity).x
            val height = (ScreenHelper.getScreen(activity).y - ScreenHelper.convertDpToPixel(
                    ScreenHelper.convertPixelsToDp((ScreenHelper.STONE_HEIGHT_DP * 2).toFloat(), it), it)).toInt()

            val left = (width / 2 - ScreenHelper.convertDpToPixel(60f, it)).toInt()
            val right = (width / 2 + ScreenHelper.convertDpToPixel(60f, it)).toInt()
            val top = (height - ScreenHelper.convertDpToPixel(120f, it)).toInt()

            val zone = Zone(left, right, top, height)
            val finger = Finger(flCrystal, activity, zone)
            finger.attachListener(object : MapInterface {
                override fun crossMiddle(state: Int) {
                    when (state) {
                        ScreenHelper.TOP -> Log.e(TAG, "Top")
                        ScreenHelper.BOTTOM -> Log.e(TAG, "Bottom")
                    }
                }

                override fun onRelease(state: Int) {
                    if (state == ScreenHelper.TOP) {
                        Thread({
                            val crystal = Crystal(DateTime().toString(DateHelper.pattern))
                            appDatabase.crystalDao().addCrystal(crystal)
                        }).start()

                        crystalCount++
                        txtCrystalCount.text = "$crystalCount"

                        Handler().postDelayed({
                            if (crystalCount == 21) {
                                val mPlayer = MediaPlayer.create(context, R.raw.fanfar)
                                mPlayer.start()

                                context?.let {
                                    val dialog = MaterialDialog.Builder(it)
                                            .customView(R.layout.dialog_fanfar, false)
                                            .build()

                                    dialog.show()
                                }
                            }

                            if (AppData.appConfiguration.isSound) {
                                MusicManager.getInstance().play(context, R.raw.bells)
                            }

                            val animator = ObjectAnimator.ofFloat(txtCrystalSmile, View.ALPHA, 0f, 1f).setDuration(750)
                            animator.addListener(object : AnimatorListenerAdapter() {
                                override fun onAnimationEnd(animation: Animator?) {
                                    super.onAnimationEnd(animation)
                                    ObjectAnimator.ofFloat(txtCrystalSmile, View.ALPHA, 1f, 0f).setDuration(750)
                                            .start()
                                }
                            })

                            if (AppData.appConfiguration.isVibration) {
                                context?.let {
                                    val v = it.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                                    // Vibrate for 500 milliseconds
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                        v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE))
                                    } else {
                                        //deprecated in API 26
                                        v.vibrate(500)
                                    }
                                }
                            }

                            if (imgCrystal != null) {
                                imgCrystal.play()
                            }
                            handler.postDelayed({
                                if (imgCrystal != null) {
                                    imgCrystal.pause()
                                }
                            }, 900)

                            when (crystalCount) {
                                4 -> {
                                    txtCrystalSmile.text = "👍"
                                    animator.start()
                                }
                                11 -> {
                                    txtCrystalSmile.text = "😁"
                                    animator.start()
                                }
                                17 -> {
                                    txtCrystalSmile.text = "🙏"
                                    animator.start()
                                }

                                25 -> showRulesDialog()
                                50 -> showRulesDialog()
                                75 -> showRulesDialog()
                                100 -> showRulesDialog()
                            }

                            when (crystalCount) {
//                                in 0..6 -> Glide.with(this@CrystalFragment).load(R.drawable.crystal_1).into(GifDrawableImageViewTarget(imgCrystal, 0))
//                                in 7..13 -> Glide.with(this@CrystalFragment).load(R.drawable.crystal_2).into(GifDrawableImageViewTarget(imgCrystal, 0))
//                                in 14..20 -> Glide.with(this@CrystalFragment).load(R.drawable.crystal_3).into(GifDrawableImageViewTarget(imgCrystal, 0))
//                                else -> Glide.with(this@CrystalFragment).load(R.drawable.crystal_4).into(GifDrawableImageViewTarget(imgCrystal, 0))
                                0 -> imgCrystal.gifResource = R.drawable.crystal_1
                                7 -> imgCrystal.gifResource = R.drawable.crystal_2
                                14 -> imgCrystal.gifResource = R.drawable.crystal_3
                                20 -> imgCrystal.gifResource = R.drawable.crystal_4
                            }
                        }, 400)
                    }
                }

                override fun onHold() {

                }
            })
        }
    }

    fun clearCrystals() {
        txtCrystalCount.text = "0"
        imgCrystal.gifResource = R.drawable.crystal_1
    }

    fun showRulesDialog() {
        context?.let {
            val materialDialog = MaterialDialog.Builder(it)
                    .title(getString(R.string.menu_about))
                    .content(getString(R.string.about_content_english))
                    .positiveText("Ok")
                    .build()
            materialDialog.show()
        }
    }
}