package ru.agladkov.appreciate.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_about_detail.*
import ru.agladkov.appreciate.R
import ru.agladkov.appreciate.adapters.AboutAdapter
import ru.agladkov.appreciate.base.BaseAdapterCallback
import ru.agladkov.appreciate.base.BaseChildFragment
import ru.agladkov.appreciate.helpers.Keys
import ru.agladkov.appreciate.helpers.ListConfig
import ru.agladkov.appreciate.helpers.ScreenKeys
import ru.agladkov.appreciate.interfaces.RouterProvider
import ru.agladkov.appreciate.models.AboutModel
import java.util.*

/**
 * Created by neura on 09.03.18.
 */
class AboutDetailFragment : BaseChildFragment() {

    companion object {
        fun getNewInstance(data: Bundle): AboutDetailFragment {
            val fragment = AboutDetailFragment()
            fragment.arguments = data
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_about_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mCallback?.updateActionBar(isBack = false, isHistory = false, title = getString(R.string.title_about))

        btnAboutDetailBack.setOnClickListener {
            (parentFragment as? RouterProvider)?.getRouter()?.exit()
        }
        if (arguments == null) {
            (parentFragment as? RouterProvider)?.getRouter()?.exit()
        } else {
            txtAboutContent.text = arguments?.getString(Keys.Content.value)
            txtAboutDetailTitle.text = arguments?.getString(Keys.Title.value)
        }
    }
}