package ru.agladkov.appreciate.fragments

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.Switch
import kotlinx.android.synthetic.main.fragment_about.*
import kotlinx.android.synthetic.main.fragment_notifications.*
import ru.agladkov.appreciate.R
import ru.agladkov.appreciate.adapters.SwitchAdapter
import ru.agladkov.appreciate.base.BaseChildFragment
import ru.agladkov.appreciate.di.App
import ru.agladkov.appreciate.helpers.AppData
import ru.agladkov.appreciate.helpers.ListConfig
import ru.agladkov.appreciate.helpers.ScreenKeys
import ru.agladkov.appreciate.interfaces.RouterProvider
import ru.agladkov.appreciate.models.SwitchModel
import ru.agladkov.appreciate.room.AppDatabase
import ru.agladkov.appreciate.room.Configuration
import java.util.*
import javax.inject.Inject

/**
 * Created by neura on 09.03.18.
 */

class NotificationsFragment : BaseChildFragment() {
    private val mAdapter = SwitchAdapter()
    private val items: MutableList<SwitchModel> = LinkedList()
    private val TAG: String = NotificationsFragment::class.java.simpleName

    companion object {
        fun getNewInstance(): NotificationsFragment {
            return NotificationsFragment()
        }
    }

    @Inject
    lateinit var appDatabase: AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(fragment = this@NotificationsFragment)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_notifications, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mCallback?.updateActionBar(isBack = false, isHistory = false, title = getString(R.string.title_notifications))
        populateItems()

        val listConfig = context?.let {
            ListConfig.Builder(mAdapter)
                    .setHasFixedSize(isFixedSize = true)
                    .setHasNestedScroll(isNestedScroll = false)
                    .build(context = it)
        }
        context?.let { listConfig?.applyConfig(context = it, recyclerView = recyclerSwitch) }
        btnNotificationsBack.setOnClickListener {
            (parentFragment as? RouterProvider)?.getRouter()?.replaceScreen(ScreenKeys.More.value)
        }

        mAdapter.attachListener(listener = object: SwitchAdapter.SwitchListener {
            override fun onSwitch(id: Int, isOn: Boolean) {
                when (id) {
                    0 -> {
                        Thread({
                            config.isNotifications = isOn
                            AppData.appConfiguration.isNotifications = isOn
                            appDatabase.configurationDao().insertConfiguration(configuration = config)
                        }).start()
                    }
                    1 -> {
                        Thread({
                            config.isSound = isOn
                            AppData.appConfiguration.isSound = isOn
                            appDatabase.configurationDao().insertConfiguration(configuration = config)
                        }).start()
                    }
                    2 -> {
                        Thread({
                            config.isVibration = isOn
                            AppData.appConfiguration.isVibration = isOn
                            appDatabase.configurationDao().insertConfiguration(configuration = config)
                        }).start()
                    }
                }
            }
        })
    }

    private lateinit var config: Configuration
    private fun populateItems() {
        items.clear()
        val handler = Handler()
        Thread({
            items.add(SwitchModel(id = 0, title = getString(R.string.sw_item_notifications), isOn = AppData.appConfiguration.isNotifications))
            items.add(SwitchModel(id = 1, title = getString(R.string.sw_item_sounds), isOn = AppData.appConfiguration.isSound))
            items.add(SwitchModel(id = 2, title = getString(R.string.sw_item_vibration), isOn = AppData.appConfiguration.isVibration))

            handler.post {
                this.config = AppData.appConfiguration
                if (mAdapter.hasItems) {
                    mAdapter.updateItems(itemsList = items)
                } else {
                    mAdapter.setList(dataList = items)
                }
            }
        }).start()


    }
}