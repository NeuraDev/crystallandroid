package ru.agladkov.appreciate.fragments

import android.Manifest
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_journal.*
import kotlinx.android.synthetic.main.fragment_month.*
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import ru.agladkov.appreciate.R
import ru.agladkov.appreciate.adapters.JournalAdapter
import ru.agladkov.appreciate.adapters.MonthAdapter
import ru.agladkov.appreciate.base.BaseAdapterCallback
import ru.agladkov.appreciate.base.BaseChildFragment
import ru.agladkov.appreciate.di.App
import ru.agladkov.appreciate.helpers.*
import ru.agladkov.appreciate.interfaces.RouterProvider
import ru.agladkov.appreciate.models.JournalModel
import ru.agladkov.appreciate.models.MonthModel
import ru.agladkov.appreciate.room.AppDatabase
import ru.terrakok.cicerone.Router
import java.io.File
import java.util.*
import android.content.Intent
import android.net.Uri
import android.support.v4.view.accessibility.AccessibilityEventCompat.setAction
import kotlinx.android.synthetic.main.fragment_day.*
import ru.agladkov.appreciate.core.ScreenHelper
import javax.inject.Inject
import android.content.pm.PackageManager
import android.Manifest.permission
import android.Manifest.permission.RECORD_AUDIO
import android.os.Build
import android.support.v4.app.ActivityCompat
import com.robertsimoes.shareable.Shareable


/**
 * Created by neura on 09.03.18.
 */

class MonthFragment : BaseChildFragment() {
    private val TAG: String = MonthFragment::class.java.simpleName
    private val mAdapter = MonthAdapter()
    private val items: MutableList<MonthModel> = LinkedList()

    companion object {
        fun getNewInstance(args: Bundle?): MonthFragment {
            val fragment = MonthFragment()
            fragment.arguments = args
            return fragment
        }
    }

    @Inject
    lateinit var appDatabase: AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(fragment = this@MonthFragment)
        super.onCreate(savedInstanceState)
        mAdapter.setList(dataList = items)
        mAdapter.attachCallback(object: BaseAdapterCallback<MonthModel> {
            override fun onItemClick(model: MonthModel, view: View) {
                if (model.recordsCount > 0) {
                    val bundle = Bundle()
                    bundle.putInt(Keys.MonthNumber.value, monthNumber)
                    bundle.putInt(Keys.YearNumber.value, yearNumber)
                    bundle.putInt(Keys.DayNumber.value, model.dayNumber)
                    (parentFragment as? RouterProvider)?.getRouter()?.navigateTo(ScreenKeys.Day.value,
                            bundle)
                } else {
                    Toast.makeText(context, getString(R.string.no_records), Toast.LENGTH_SHORT).show()
                }
            }

            override fun onLongClick(model: MonthModel, view: View): Boolean {
                return true
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_month, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mCallback?.updateActionBar(isBack = false, isHistory = false, title = getString(R.string.title_journal))

        val listConfig = context?.let {
            ListConfig.Builder(mAdapter)
                    .setHasFixedSize(isFixedSize = true)
                    .setHasNestedScroll(isNestedScroll = true)
                    .build(context = it)
        }
        context?.let { listConfig?.applyConfig(context = it, recyclerView = recyclerMonth) }

        txtMonthAllDay.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt(Keys.MonthNumber.value, monthNumber)
            bundle.putInt(Keys.YearNumber.value, yearNumber)
            (parentFragment as? RouterProvider)?.getRouter()?.navigateTo(ScreenKeys.DayAll.value,
                    bundle)
        }
        btnMonthBack.setOnClickListener {
            (parentFragment as? RouterProvider)?.getRouter()?.exit()
        }

        btnMonthShare.setOnClickListener {
            activity?.let {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (it.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                            || it.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(it, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE), 1)
                        return@setOnClickListener
                    }
                }

                Share.share(activity = it)
            }
        }

        btnMonthPdf.setOnClickListener {
            activity?.let {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (it.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                            || it.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(it, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE), 1)
                        return@setOnClickListener
                    }
                }

                val pdfHelper = PdfHelper(recyclerMonth, ScreenHelper.getScreen(it).x.toLong(),
                        ScreenHelper.getScreen(it).y.toLong())
                val file = pdfHelper.saveImageToPDF()

                val sharingIntent = Intent(Intent.ACTION_SEND)
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here")
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.app_name))
                sharingIntent.putExtra(android.content.Intent.EXTRA_STREAM, Uri.parse("file://${file.absolutePath}"))
                sharingIntent.type = "application/pdf"
                it.startActivity(Intent.createChooser(sharingIntent, it.resources?.getString(R.string.share_using)))
            }
        }

        populateItems(args = arguments)
    }

    var monthNumber = 0
    var yearNumber = 0
    private fun populateItems(args: Bundle?) {
        monthNumber = args?.getInt(Keys.MonthNumber.value) ?: 0
        yearNumber = args?.getInt(Keys.YearNumber.value) ?: 0

        val dateFormatter = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm:ss")

        if (monthNumber == 0 || yearNumber == 0) {
            (parentFragment as? RouterProvider)?.getRouter()?.exit()
        } else {
            txtMonthTitle.text = DateHelper.monthForNumber(context = context, monthNumber = monthNumber)

            val handler = Handler()
            Thread({
                val crystals = appDatabase.crystalDao().all
                        .filter { dateFormatter.parseDateTime(it.dateTime).year == yearNumber }
                        .filter { dateFormatter.parseDateTime(it.dateTime).monthOfYear == monthNumber }

                val sortedRecords = appDatabase.recordDao().all
                        .filter { dateFormatter.parseDateTime(it.dateTime).year == yearNumber }
                        .filter { dateFormatter.parseDateTime(it.dateTime).monthOfYear == monthNumber }
                        .sortedWith(compareBy({ dateFormatter.parseDateTime(it.dateTime) })).reversed()

                val days = DateHelper.extractDaysCount(list = sortedRecords)
                val daysCrystal = DateHelper.extractDaysCrystalsCount(list = crystals)

                daysCrystal.keys.forEach {
                    if (days[it] ?: 0 == 0) {
                        days.put(it, 0)
                    }
                }

                items.clear()
                days.keys.forEach({
                    val percent = (crystals.filter { crystal -> dateFormatter.parseDateTime(crystal.dateTime)
                            .dayOfMonth == it }.count() / 21f) * 100f
                    items.add(MonthModel(id = it, dayNumber = it,
                            monthName = DateHelper.monthForNumber(context = context, monthNumber = monthNumber),
                            recordsCount = days[it] ?: 0, percent = percent.toInt()))
                })

                handler.post {
                    if (mAdapter.hasItems) {
                        mAdapter.updateItems(itemsList = items)
                    } else {
                        mAdapter.setList(dataList = items)
                    }
                }
            }).start()
        }
    }
}