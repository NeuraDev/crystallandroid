package ru.agladkov.appreciate.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_report.*
import kotlinx.android.synthetic.main.fragment_about.*
import ru.agladkov.appreciate.R
import ru.agladkov.appreciate.adapters.AboutAdapter
import ru.agladkov.appreciate.base.BaseAdapter
import ru.agladkov.appreciate.base.BaseAdapterCallback
import ru.agladkov.appreciate.base.BaseChildFragment
import ru.agladkov.appreciate.helpers.Keys
import ru.agladkov.appreciate.helpers.ListConfig
import ru.agladkov.appreciate.helpers.ScreenKeys
import ru.agladkov.appreciate.interfaces.RouterProvider
import ru.agladkov.appreciate.models.AboutModel
import java.util.*

/**
 * Created by neura on 09.03.18.
 */
class AboutFragment: BaseChildFragment() {
    private val mAdapter = AboutAdapter()
    private val dataList: MutableList<AboutModel> = LinkedList()

    companion object {
        fun getNewInstance(): AboutFragment {
            return AboutFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        populateItems()
        mAdapter.setList(dataList = dataList)
        mAdapter.attachCallback(object: BaseAdapterCallback<AboutModel> {
            override fun onItemClick(model: AboutModel, view: View) {
                val bundle = Bundle()
                bundle.putString(Keys.Title.value, model.title)
                when (model.id) {
                    0 -> bundle.putString(Keys.Content.value, getString(R.string.about_content_english))
                    1 -> bundle.putString(Keys.Content.value, getString(R.string.about_content_spanish))
                    2 -> bundle.putString(Keys.Content.value, getString(R.string.about_content_deutch))
                    3 -> bundle.putString(Keys.Content.value, getString(R.string.about_content_russian))
                    4 -> bundle.putString(Keys.Content.value, getString(R.string.about_content_italian))
                    5 -> bundle.putString(Keys.Content.value, getString(R.string.about_content_chinese))
                    6 -> bundle.putString(Keys.Content.value, getString(R.string.about_content_french))
                    7 -> bundle.putString(Keys.Content.value, getString(R.string.about_content_portugese))
                }
                (parentFragment as RouterProvider).getRouter().navigateTo(ScreenKeys.AboutDetail.value, bundle)
            }

            override fun onLongClick(model: AboutModel, view: View): Boolean {
                return true
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_about, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mCallback?.updateActionBar(isBack = false, isHistory = false, title = getString(R.string.title_about))

        val listConfig = context?.let {
            ListConfig.Builder(mAdapter)
                    .setHasFixedSize(isFixedSize = true)
                    .setHasNestedScroll(isNestedScroll = false)
                    .build(context = it)
        }

        btnAboutBack.setOnClickListener { (parentFragment as? RouterProvider)?.getRouter()?.exit() }
        context?.let { listConfig?.applyConfig(context = it, recyclerView = recyclerAbout) }
    }

    private fun populateItems() {
        dataList.add(AboutModel(id = 0, title = "English"))
        dataList.add(AboutModel(id = 1, title = "Español"))
        dataList.add(AboutModel(id = 2, title = "Deutsch"))
        dataList.add(AboutModel(id = 3, title = "Русский"))
        dataList.add(AboutModel(id = 4, title = "Italiano"))
        dataList.add(AboutModel(id = 5, title = "中国"))
        dataList.add(AboutModel(id = 6, title = "Français"))
        dataList.add(AboutModel(id = 7, title = "Portugues"))
    }
}