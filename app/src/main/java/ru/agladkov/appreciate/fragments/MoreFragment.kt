package ru.agladkov.appreciate.fragments

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_more.*
import ru.agladkov.appreciate.R
import ru.agladkov.appreciate.base.BaseChildFragment
import ru.agladkov.appreciate.di.App
import ru.agladkov.appreciate.helpers.AppData
import ru.agladkov.appreciate.helpers.ScreenKeys
import ru.agladkov.appreciate.helpers.Share
import ru.agladkov.appreciate.interfaces.RouterProvider
import ru.agladkov.appreciate.room.AppDatabase
import javax.inject.Inject

/**
 * Created by neura on 09.03.18.
 */

class MoreFragment : BaseChildFragment() {

    companion object {
        fun getNewInstance(): MoreFragment {
            return MoreFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(fragment =this@MoreFragment)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_more, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mCallback?.updateActionBar(isBack = false, isHistory = false, title = getString(R.string.title_about))

        val router = (parentFragment as RouterProvider).getRouter()
        llMoreNotifications.setOnClickListener { router.replaceScreen(ScreenKeys.Notifications.value) }
        llMoreAbout.setOnClickListener { router.navigateTo(ScreenKeys.About.value) }
        llMoreCommend.setOnClickListener {
            activity?.let {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (it.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                            || it.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(it, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE), 1)
                        return@setOnClickListener
                    }
                }

                Share.share(it)
            }
        }
        llMoreDonate.setOnClickListener { router.navigateTo(ScreenKeys.Donate.value) }
        llMoreRate.setOnClickListener { router.navigateTo(ScreenKeys.Rate.value) }
    }
}