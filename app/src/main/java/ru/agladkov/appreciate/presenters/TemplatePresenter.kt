package ru.agladkov.appreciate.presenters

import android.os.Handler
import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.agladkov.appreciate.room.AppDatabase
import ru.agladkov.appreciate.views.TemplateView

/**
 * Created by neura on 11.03.18.
 */
@InjectViewState
class TemplatePresenter(private val appDatabase: AppDatabase): MvpPresenter<TemplateView>() {
    private val TAG: String = TemplatePresenter::class.java.simpleName
    private val handler = Handler()

    fun loadTemplates() {
        Thread({
            val templates = appDatabase.templateDao().getAllTemplates()
            if (templates.isEmpty()) {
                handler.post {
                    viewState.setupNoItems()
                }
            } else {
                handler.post {
                    viewState.setupItems(data = templates)
                }
            }
        }).start()
    }
}