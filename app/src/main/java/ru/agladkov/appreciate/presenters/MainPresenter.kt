package ru.agladkov.appreciate.presenters

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.agladkov.appreciate.helpers.ScreenKeys
import ru.agladkov.appreciate.views.MainView
import ru.terrakok.cicerone.Router

/**
 * Created by neura on 09.03.18.
 */
@InjectViewState
class MainPresenter(private val routerMain: Router): MvpPresenter<MainView>() {
    private var lastTab = 0

    fun switchFragment(case: Int) {
        when (case) {
            0 -> routerMain.replaceScreen(ScreenKeys.Main.value)
            1 -> routerMain.replaceScreen(ScreenKeys.Journal.value)
            2 -> routerMain.replaceScreen(ScreenKeys.More.value)
        }

        if (lastTab == case) viewState.performDoubleTap(case = lastTab)
        lastTab = case
    }

    fun onBackClick() {
        routerMain.exit()
    }
}