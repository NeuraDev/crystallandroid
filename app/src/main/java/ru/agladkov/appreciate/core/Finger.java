package ru.agladkov.appreciate.core;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

import com.cunoraz.gifview.library.GifView;

import pl.droidsonroids.gif.GifImageView;
import ru.agladkov.appreciate.interfaces.MapInterface;

/**
 * Created by agladkov on 17.01.18.
 */

public class Finger {
    private String TAG = Finger.class.getSimpleName();
    private View container;
    private Activity activity;
    private MapInterface listener;
    private Stone stone;
    private Zone zone;

    public Finger(View container, Activity activity, Zone zone) {
        this.container = container;
        this.activity = activity;
        this.zone = zone;
        initInteraction();
    }

    public void attachListener(MapInterface listener) {
        this.listener = listener;
    }

    public void detachListener() {
        this.listener = null;
    }

    private void initInteraction() {
        Stone stone = null;
        if (container instanceof FrameLayout) {
            stone = new Stone((FrameLayout) container);
        }

        final Stone finalStone = stone;
        container.setOnTouchListener(new View.OnTouchListener() {
            private float rawX = 0;
            private float rawY = 0;
            private int height = (int) (ScreenHelper.getScreen(activity).y - ScreenHelper.
                    convertDpToPixel(ScreenHelper.convertPixelsToDp(ScreenHelper.STONE_HEIGHT_DP * 2, activity), activity));
            private int width = ScreenHelper.getScreen(activity).x;
            private int state = ScreenHelper.TOP;
            private View currentStone = null;


            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (listener == null) return false;
                if (finalStone == null) return false;
                rawX = motionEvent.getRawX();
                rawY = motionEvent.getRawY();
//                Log.e(TAG, "rawY - " + rawY + " height - " + (height / 2));

                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (zone.checkBorder(rawX, rawY)) {
                            currentStone = finalStone.generateStone(rawX, rawY);
                            state = ScreenHelper.BOTTOM;
                            listener.onHold();
                        }
                        break;

                    case MotionEvent.ACTION_MOVE:
                        finalStone.move(currentStone, rawX, rawY);
                        if (rawY > (height / 1.4)) {
                            if (state == ScreenHelper.TOP) {
                                finalStone.shutdown(currentStone);
                                state = ScreenHelper.BOTTOM;
                                listener.crossMiddle(ScreenHelper.BOTTOM);
                            }
                        } else {
                            if (state == ScreenHelper.BOTTOM) {
                                finalStone.highlight(currentStone);
                                state = ScreenHelper.TOP;
                                listener.crossMiddle(ScreenHelper.TOP);
                            }
                        }
                        break;

                    case MotionEvent.ACTION_CANCEL:
                    case MotionEvent.ACTION_UP:
                        if (currentStone != null) {
                            finalStone.finishMove(currentStone, state, height, width, rawX, rawY);
                            currentStone = null;
                            listener.onRelease(state);
                        }
                        break;
                }

                return true;
            }
        });
    }
}
