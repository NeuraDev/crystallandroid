package ru.agladkov.appreciate.core;

import android.animation.ValueAnimator;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.cunoraz.gifview.library.GifView;

import pl.droidsonroids.gif.GifImageView;
import ru.agladkov.appreciate.R;
import ru.agladkov.appreciate.helpers.MyGifView;
import ru.agladkov.appreciate.helpers.WindowUtils;

/**
 * Created by agladkov on 17.01.18.
 */

public class Stone {;
    private FrameLayout container;
    private int width;
    private int height;
    private final int MAX_STONE_COUNT = 21;
    private int stoneCount = 0;

    public Stone(FrameLayout container) {
        this.container = container;

        width = (int) ScreenHelper.convertDpToPixel(ScreenHelper.STONE_WIDTH_DP, container.getContext());
        height = (int) ScreenHelper.convertDpToPixel(ScreenHelper.STONE_WIDTH_DP, container.getContext());
    }

    View generateStone(float startX, float startY) {
        if (stoneCount <= MAX_STONE_COUNT) {
            ImageView view = new ImageView(container.getContext());
            Glide.with(container.getContext()).load(R.mipmap.mini_rock).into(view);

            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(width, height);
            params.leftMargin = (int) startX;
            params.topMargin = (int) startY;
            view.setLayoutParams(params);
            container.addView(view);

            stoneCount++;
            return view;
        }

        return null;
    }

    private float newX = 0;
    private float newY = 0;
    void move(View stone, float newX, float newY) {
        if (stone == null) return;
        this.newX = newX;
        this.newY = newY;

        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) stone.getLayoutParams();
        params.leftMargin = (int) newX;
        params.topMargin = (int) newY;
        stone.setLayoutParams(params);
    }

    void finishMove(final View stone, int state, int height, int width, float currentX, final float currentY) {
        if (stone == null) return;

        ValueAnimator alphaAnimator = ValueAnimator.ofFloat(1, 0);
        alphaAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                stone.setAlpha((Float) valueAnimator.getAnimatedValue());
            }
        });

        int DURATION_TIME = 1000;
        alphaAnimator.setDuration(DURATION_TIME + 1);
        alphaAnimator.start();

        switch (state) {
            case ScreenHelper.TOP:
                ValueAnimator yAnimator = ValueAnimator.ofInt((int) currentY,
                        (int) WindowUtils.Companion.convertDpToPixel(196f / 2f, container.getContext()));
                ValueAnimator xAnimator = ValueAnimator.ofInt((int) currentX, width / 2);

                xAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) stone.getLayoutParams();
                        params.leftMargin = (int) valueAnimator.getAnimatedValue();
                        stone.setLayoutParams(params);
                    }
                });

                yAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) stone.getLayoutParams();
                        params.topMargin = (int) valueAnimator.getAnimatedValue();
                        stone.setLayoutParams(params);
                    }
                });

                xAnimator.setDuration(DURATION_TIME);
                xAnimator.start();
                yAnimator.setDuration(DURATION_TIME);
                yAnimator.start();
                break;

            case ScreenHelper.BOTTOM:
                yAnimator = ValueAnimator.ofInt((int) currentY, height);
                yAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) stone.getLayoutParams();
                        params.topMargin = (int) valueAnimator.getAnimatedValue();
                        stone.setLayoutParams(params);
                    }
                });
                yAnimator.setDuration(DURATION_TIME);
                yAnimator.start();
                break;
        }
    }

    public void highlight(View currentStone) {
        if (currentStone == null) return;
        if (currentStone instanceof ImageView) {
            Glide.with(container.getContext()).load(R.mipmap.mini_crystal).into(((ImageView) currentStone));
        }

        final GifView poofImage = new GifView(container.getContext());
        poofImage.setGifResource(R.drawable.anima_exp_1);

        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(width * 2, height * 2);
        params.topMargin = (int) newY - height;
        params.leftMargin = (int) newX - (width / 2);
        poofImage.setLayoutParams(params);

        container.addView(poofImage);
        poofImage.play();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                container.removeView(poofImage);
            }
        }, 500);
    }

    public void shutdown(View currentStone) {
        if (currentStone == null) return;
        if (currentStone instanceof ImageView) {
            Glide.with(container.getContext()).load(R.mipmap.mini_rock).into(((ImageView) currentStone));
        }
    }
}
