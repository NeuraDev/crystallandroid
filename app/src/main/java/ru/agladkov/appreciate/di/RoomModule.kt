package ru.agladkov.appreciate.di

import android.content.Context
import dagger.Module
import dagger.Provides
import ru.agladkov.appreciate.room.AppDatabase
import javax.inject.Singleton

/**
 * Created by neura on 09.03.18.
 */
@Module
class RoomModule {

    @Provides
    @Singleton
    fun provideRoomCurrencyDataSource(context: Context) =
            AppDatabase.buildDataSource(context)
}