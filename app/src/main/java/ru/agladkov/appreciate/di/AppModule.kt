package ru.agladkov.appreciate.di

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by neura on 09.03.18.
 */
@Module
class AppModule(private val app: App) {
    @Provides @Singleton fun provideContext(): Context = app
}