package ru.agladkov.appreciate.di

import dagger.Module
import dagger.Provides
import ru.agladkov.appreciate.navigation.LocalCiceroneHolder
import javax.inject.Singleton

/**
 * Created by neura on 09.03.18.
 */
@Module
class LocalNavigationModule {

    @Provides
    @Singleton
    fun provideLocalNavigationHolder(): LocalCiceroneHolder {
        return LocalCiceroneHolder()
    }
}