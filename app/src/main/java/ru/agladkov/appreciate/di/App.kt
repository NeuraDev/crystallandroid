package ru.agladkov.appreciate.di

import android.app.Application

/**
 * Created by neura on 09.03.18.
 */
class App : Application() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        initializeDagger()
    }

    private fun initializeDagger() {
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(app = this@App))
                .roomModule(RoomModule())
                .localNavigationModule(LocalNavigationModule())
                .build()
    }
}
