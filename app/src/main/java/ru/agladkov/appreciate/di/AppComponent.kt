package ru.agladkov.appreciate.di

import dagger.Component
import ru.agladkov.appreciate.MainActivity
import ru.agladkov.appreciate.base.BaseContainer
import ru.agladkov.appreciate.fragments.*
import javax.inject.Singleton

/**
 * Created by neura on 09.03.18.
 */
@Component(modules = [(AppModule::class), (RoomModule::class), (NavigationModule::class), (LocalNavigationModule::class)])
@Singleton
interface AppComponent {
    // Activities
    fun inject(activity: MainActivity)

    // Fragments
    fun inject(fragment: CrystalFragment)
    fun inject(fragment: BaseContainer)
    fun inject(fragment: TemplateFragment)
    fun inject(fragment: TemplateAddFragment)
    fun inject(fragment: TemplateEditDeleteFragment)
    fun inject(fragment: TemplateEditFragment)
    fun inject(fragment: AddCommentFragment)
    fun inject(fragment: JournalFragment)
    fun inject(fragment: MonthFragment)
    fun inject(fragment: DayFragment)
    fun inject(fragment: MoreFragment)
    fun inject(fragment: NotificationsFragment)
    fun inject(fragment: AllDayFragment)
}