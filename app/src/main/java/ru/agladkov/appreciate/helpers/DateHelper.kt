package ru.agladkov.appreciate.helpers

import android.content.Context
import org.joda.time.format.DateTimeFormat
import ru.agladkov.appreciate.R
import ru.agladkov.appreciate.models.JournalModel
import ru.agladkov.appreciate.room.Crystal
import ru.agladkov.appreciate.room.Record

/**
 * Created by neura on 12.03.18.
 */
class DateHelper {
    companion object {
        val pattern = "dd.MM.yyyy HH:mm:ss"

        fun caseMonthForNumber(context: Context?, monthNumber: Int): String {
            context?.let {
                return when (monthNumber) {
                    2 -> context.getString(R.string.monthFeb_case)
                    3 -> context.getString(R.string.monthMar_case)
                    4 -> context.getString(R.string.monthApr_case)
                    5 -> context.getString(R.string.monthMay_case)
                    6 -> context.getString(R.string.monthJun_case)
                    7 -> context.getString(R.string.monthJul_case)
                    8 -> context.getString(R.string.monthAug_case)
                    9 -> context.getString(R.string.monthSep_case)
                    10 -> context.getString(R.string.monthOct_case)
                    11 -> context.getString(R.string.monthNov_case)
                    12 -> context.getString(R.string.monthDec_case)
                    else -> context.getString(R.string.monthJan_case)
                }
            }

            return ""
        }

        fun monthForNumber(context: Context?, monthNumber: Int): String {
            context?.let {
                return when(monthNumber) {
                    2 -> context.getString(R.string.monthFeb)
                    3 -> context.getString(R.string.monthMar)
                    4 -> context.getString(R.string.monthApr)
                    5 -> context.getString(R.string.monthMay)
                    6 -> context.getString(R.string.monthJun)
                    7 -> context.getString(R.string.monthJul)
                    8 -> context.getString(R.string.monthAug)
                    9 -> context.getString(R.string.monthSep)
                    10 -> context.getString(R.string.monthOct)
                    11 -> context.getString(R.string.monthNov)
                    12 -> context.getString(R.string.monthDec)
                    else -> context.getString(R.string.monthJan)
                }
            }

            return ""
        }

        fun extractMonthCountForYear(year: Int, list: List<Record>): Map<Int, Int> {
            val dateFormat = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm:ss")
            return list.asSequence()
                    .filter { record -> dateFormat.parseDateTime(record.dateTime).year == year }
                    .groupingBy { dateFormat.parseDateTime(it.dateTime).monthOfYear }
                    .eachCount().filter { it.value >= 1 }
        }

        fun extractMonthCountForYearJournal(year: Int, list: List<JournalModel>): Map<Int, Int> {
            return list.asSequence()
                    .filter { record -> record.year == year }
                    .groupingBy { it.month }
                    .eachCount().filter { it.value >= 1 }
        }

        fun extractYearsForCrystals(crystals: List<Crystal>): Map<Int, Int> {
            val dateFormat = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm:ss")
            return crystals.asSequence()
                    .groupingBy { dateFormat.parseDateTime(it.dateTime).year }
                    .eachCount().filter { it.value >= 1 }
        }

        fun extractYearsForRecords(records: List<Record>): Map<Int, Int> {
            val dateFormat = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm:ss")
            return records.asSequence()
                    .groupingBy { dateFormat.parseDateTime(it.dateTime).year }
                    .eachCount().filter { it.value >= 1 }
        }

        fun extractYearsForJournal(items: List<JournalModel>): Map<Int, Int> {
            return items.asSequence()
                    .groupingBy { it.year }
                    .eachCount().filter { it.value >= 1 }
        }

        fun extractMonthCountForYearC(year: Int, crystal: List<Crystal>): Map<Int, Int> {
            val dateFormat = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm:ss")
            return crystal.asSequence()
                    .filter { crys -> dateFormat.parseDateTime(crys.dateTime).year == year }
                    .groupingBy { dateFormat.parseDateTime(it.dateTime).monthOfYear }
                    .eachCount().filter { it.value >= 1 }
        }

        fun extractDaysCount(list: List<Record>): MutableMap<Int, Int> {
            val dateFormat = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm:ss")
            return list.asSequence()
                    .groupingBy { dateFormat.parseDateTime(it.dateTime).dayOfMonth }
                    .eachCount().filter { it.value >= 1 }.toMutableMap()
        }

        fun extractDaysCrystalsCount(list: List<Crystal>): Map<Int, Int> {
            val dateFormat = DateTimeFormat.forPattern(pattern)
            return list.asSequence()
                    .groupingBy { dateFormat.parseDateTime(it.dateTime).dayOfMonth }
                    .eachCount().filter { it.value >= 1 }
        }
    }
}