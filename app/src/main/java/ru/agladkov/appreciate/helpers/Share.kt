package ru.agladkov.appreciate.helpers

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.net.Uri
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Rect
import android.graphics.pdf.PdfDocument
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat.startActivity
import android.util.Log
import android.view.View
import ru.agladkov.appreciate.R
import java.io.ByteArrayOutputStream
import android.graphics.Bitmap.CompressFormat
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import android.os.Build
import android.os.Environment
import java.io.File
import java.io.FileOutputStream
import android.provider.MediaStore
import android.os.Environment.DIRECTORY_PICTURES
import android.os.Environment.getExternalStoragePublicDirectory
import android.support.v4.app.ActivityCompat
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import java.io.OutputStream


/**
 * Created by neura on 20.03.18.
 */
class Share {
    companion object {
        private val TAG = Share::class.java.simpleName

        @SuppressLint("SetWorldReadable")
        fun share(activity: Activity?) {
            activity?.let {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (it.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(it, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
                    }
                }

                Glide.with(it.applicationContext).asBitmap().load(R.drawable.ic_launcher).into(object: SimpleTarget<Bitmap>(1024, 1024) {
                    override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                        val sdcard = Environment.getExternalStorageDirectory()
                        if (sdcard != null) {
                            val mediaDir = File(sdcard, "DCIM/Camera")
                            if (!mediaDir.exists()) {
                                mediaDir.mkdirs()
                            }
                        }

                        val bitmapPath = MediaStore.Images.Media.insertImage(it.contentResolver, resource, "title", "description")
                        val bitmapUri = Uri.parse(bitmapPath)

                        val sharingIntent = Intent(android.content.Intent.ACTION_SEND)
                        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "")
                        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, it.getString(R.string.commend_text))
                        sharingIntent.putExtra(android.content.Intent.EXTRA_STREAM, bitmapUri)
                        sharingIntent.type = "image/*"

                        it.startActivity(Intent.createChooser(sharingIntent, it.getString(R.string.share_using)))
                    }
                })
            }
        }

        fun sharePdf(activity: Activity?, pdfFile: ByteArrayOutputStream) {
            val mShareIntent = Intent()
            mShareIntent.action = Intent.ACTION_SEND
            mShareIntent.type = "application/pdf"
            // Assuming it may go via eMail:
            mShareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, activity?.getString(R.string.share_using))
            mShareIntent.putExtra(android.content.Intent.EXTRA_STREAM, pdfFile.toByteArray())
            activity?.startActivity(mShareIntent)
        }
    }
}