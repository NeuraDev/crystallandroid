package ru.agladkov.appreciate.helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import org.joda.time.DateTime;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import ru.agladkov.appreciate.R;

/**
 * Created by neura on 20.03.18.
 */

public class PdfHelper {
    private final RecyclerView recyclerView;
    private Long width;
    private Long height;

    public PdfHelper(RecyclerView recyclerView, Long width, Long height) {
        this.recyclerView = recyclerView;
        this.width = width;
        this.height = height;
    }

    private Bitmap alternativeRecyclerViewScreenshot() {
        Bitmap bitmap = Bitmap.createBitmap(recyclerView.getWidth(),
                recyclerView.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);

        Paint paint = new Paint();
        paint.setColor(Color.parseColor("#f1f1f1"));
        paint.setStyle(Paint.Style.FILL);

        canvas.drawPaint(paint);
        recyclerView.draw(canvas);
        return bitmap;
    }

    private Bitmap getRecyclerViewScreenshot() {
        int size = recyclerView.getAdapter().getItemCount();
        RecyclerView.ViewHolder holder = recyclerView.getAdapter().createViewHolder(recyclerView, 0);
        recyclerView.getAdapter().onBindViewHolder(holder, 0);
        holder.itemView.measure(View.MeasureSpec.makeMeasureSpec(recyclerView.getWidth(), View.MeasureSpec.EXACTLY),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        holder.itemView.layout(0, 0, holder.itemView.getMeasuredWidth(), holder.itemView.getMeasuredHeight());
        Bitmap bigBitmap = Bitmap.createBitmap(recyclerView.getMeasuredWidth(), holder.itemView.getMeasuredHeight() * size,
                Bitmap.Config.ARGB_8888);

        Canvas bigCanvas = new Canvas(bigBitmap);
//        bigCanvas.drawColor(Color.GRAY);

        Bitmap icon = BitmapFactory.decodeResource(recyclerView.getContext().getResources(),
                R.mipmap.background_comments);
        bigCanvas.drawColor(recyclerView.getContext().getResources().getColor(R.color.lightGray));

        Paint paint = new Paint();
        int iHeight = 0;
        holder.itemView.setDrawingCacheEnabled(true);
        holder.itemView.buildDrawingCache();
        bigCanvas.drawBitmap(holder.itemView.getDrawingCache(), 0f, iHeight, paint);
        holder.itemView.setDrawingCacheEnabled(false);
        holder.itemView.destroyDrawingCache();
        iHeight += holder.itemView.getMeasuredHeight();
        for (int i = 1; i < size; i++) {
            recyclerView.getAdapter().onBindViewHolder(holder, i);
            holder.itemView.setDrawingCacheEnabled(true);
            holder.itemView.buildDrawingCache();
            bigCanvas.drawBitmap(holder.itemView.getDrawingCache(), 0f, iHeight, paint);
            iHeight += holder.itemView.getMeasuredHeight();
            holder.itemView.setDrawingCacheEnabled(false);
            holder.itemView.destroyDrawingCache();
        }

        return bigBitmap;
    }

    public File saveImageToPDF() {
        Bitmap bitmap = alternativeRecyclerViewScreenshot();
        PdfDocument document = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(bitmap.getWidth(), bitmap.getHeight(), 1).create();
        PdfDocument.Page page = document.startPage(pageInfo);
        Canvas canvas = page.getCanvas();

        canvas.drawBitmap(bitmap, null, new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight()), null);

        document.finishPage(page);
        File file = new File(Environment.getExternalStorageDirectory() + "/" + new DateTime()
                .toString("dd-MM-YYYY") + ".pdf");

        try {
            FileOutputStream out = new FileOutputStream(file);
            document.writeTo(out);
            document.close();
            out.close();
            return file;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
