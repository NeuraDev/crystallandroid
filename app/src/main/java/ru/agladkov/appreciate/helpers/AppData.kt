package ru.agladkov.appreciate.helpers

import ru.agladkov.appreciate.room.Configuration


/**
 * Created by neura on 14.03.18.
 */
class AppData {
    companion object {
        var template = ""
        var appConfiguration = Configuration(id = 0, isVibration = true, isNotifications = true, isSound = true)
    }
}