package ru.agladkov.appreciate.helpers

import com.anjlab.android.iab.v3.BillingProcessor

/**
 * Created by neura on 04.04.18.
 */
interface BillingProvider {
    fun getBilling(): BillingProcessor
}