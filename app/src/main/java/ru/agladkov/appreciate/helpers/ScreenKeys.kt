package ru.agladkov.appreciate.helpers

/**
 * Created by neura on 09.03.18.
 */
enum class ScreenKeys(val value: String) {
    Main("MainKey"), Journal("JournalKey"), More("MoreKey"), Crystal("CrystalKey"),
    About("AboutKey"), Notifications("NotificationsKey"), Commend("CommendKey"),
    Rate("RateKey"), Donate("DonateKey"), AboutDetail("AboutDetailKey"), Month("MonthKey"),
    Day("DayKey"), AddComment("AddCommentKey"), Template("TemplateKey"), TemplateAdd("TemplateAddKey"),
    TemplateEditDelete("TemplateEditDeleteKey"), TemplateEdit("TemplateEditKey"), DayAll("DayAllKey")
}