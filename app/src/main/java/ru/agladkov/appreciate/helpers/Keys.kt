package ru.agladkov.appreciate.helpers

/**
 * Created by neura on 09.03.18.
 */
enum class Keys(val value: String) {
    Name("KeyName"), Index("IndexName"), Template("TemplateName"), MonthNumber("MonthName"),
    YearNumber("YearName"), DayNumber("DayName"), Title("TitleName"), Content("ContentName")
}