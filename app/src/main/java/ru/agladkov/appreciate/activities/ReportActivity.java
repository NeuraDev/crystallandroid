package ru.agladkov.appreciate.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.List;

import ru.agladkov.appreciate.R;
import ru.agladkov.appreciate.adapters.RecordAdapter;
import ru.agladkov.appreciate.room.Record;

/**
 * Created by agladkov on 17.01.18.
 */

public class ReportActivity extends AppCompatActivity {
    private final String TAG = ReportActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        final Handler handler = new Handler();
        final RecordAdapter mAdapter = new RecordAdapter();
        final RecyclerView mRecycler = findViewById(R.id.recyclerReport);
        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
    }
}
