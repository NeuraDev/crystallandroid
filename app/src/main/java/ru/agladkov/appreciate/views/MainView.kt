package ru.agladkov.appreciate.views

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * Created by neura on 09.03.18.
 */
@StateStrategyType(value = AddToEndSingleStrategy::class)
interface MainView: MvpView {
    fun performDoubleTap(case: Int)
}