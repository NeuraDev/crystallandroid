package ru.agladkov.appreciate.views

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.agladkov.appreciate.room.TemplateModel

/**
 * Created by neura on 11.03.18.
 */
@StateStrategyType(value = AddToEndSingleStrategy::class)
interface TemplateView: MvpView {
    fun setupNoItems()
    fun setupItems(data: List<TemplateModel>)
}