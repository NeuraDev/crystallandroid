package ru.agladkov.appreciate;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

import javax.inject.Inject;

import ru.agladkov.appreciate.activities.ReportActivity;
import ru.agladkov.appreciate.core.Finger;
import ru.agladkov.appreciate.core.ScreenHelper;
import ru.agladkov.appreciate.core.Zone;
import ru.agladkov.appreciate.di.App;
import ru.agladkov.appreciate.fragments.containers.JournalContainer;
import ru.agladkov.appreciate.fragments.containers.MainContainer;
import ru.agladkov.appreciate.fragments.containers.SettingsContainer;
import ru.agladkov.appreciate.helpers.BillingProvider;
import ru.agladkov.appreciate.helpers.BottomNavigationHelper;
import ru.agladkov.appreciate.helpers.ScreenKeys;
import ru.agladkov.appreciate.interfaces.BackButtonListener;
import ru.agladkov.appreciate.interfaces.FragmentView;
import ru.agladkov.appreciate.interfaces.KeyboardHandler;
import ru.agladkov.appreciate.interfaces.MapInterface;
import ru.agladkov.appreciate.interfaces.RouterProvider;
import ru.agladkov.appreciate.interfaces.TemplateHandler;
import ru.agladkov.appreciate.presenters.MainPresenter;
import ru.agladkov.appreciate.room.Record;
import ru.agladkov.appreciate.views.MainView;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;
import ru.terrakok.cicerone.commands.Back;
import ru.terrakok.cicerone.commands.Command;
import ru.terrakok.cicerone.commands.Replace;
import ru.terrakok.cicerone.commands.SystemMessage;

public class MainActivity extends MvpAppCompatActivity implements MainView, FragmentView, RouterProvider,
        KeyboardHandler, BillingProvider, BillingProcessor.IBillingHandler {
    private static String TAG = MainActivity.class.getSimpleName();

    @InjectPresenter
    MainPresenter mainPresenter;

    @ProvidePresenter
    MainPresenter createMainPresenter() {
        return new MainPresenter(routerMain);
    }

    @Inject
    NavigatorHolder navigatorHolder;

    @Inject
    Router routerMain;

    private MainContainer mainContainer;
    private JournalContainer journalContainer;
    private SettingsContainer settingsContainer;

    private BillingProcessor bp;

    private String mainTAG = MainContainer.Companion.getTAG();
    private String journalTAG = JournalContainer.Companion.getTAG();
    private String settingsTag = SettingsContainer.Companion.getTAG();

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.navigation_main:
                            mainPresenter.switchFragment(0);
                            break;

                        case R.id.navigation_journal:
                            mainPresenter.switchFragment(1);
                            break;

                        case R.id.navigation_settings:
                            mainPresenter.switchFragment(2);
                            break;
                    }
                    return true;
                }
            };

    private Navigator navigator = new Navigator() {
        @Override
        public void applyCommands(Command[] commands) {
            for (Command command : commands) applyCommand(command);
        }

        void applyCommand(Command command) {
            if (command instanceof Back) {
                finish();
            } else if (command instanceof SystemMessage) {
                Toast.makeText(MainActivity.this, ((SystemMessage) command).getMessage(), Toast.LENGTH_SHORT).show();
            } else if (command instanceof Replace) {
                FragmentManager fm = getSupportFragmentManager();
                switch (((Replace) command).getScreenKey()) {
                    case "MainKey":
                        fm.beginTransaction()
                                .detach(journalContainer)
                                .detach(settingsContainer)
                                .attach(mainContainer)
                                .commitNow();
                        break;

                    case "JournalKey":
                        fm.beginTransaction()
                                .detach(mainContainer)
                                .detach(settingsContainer)
                                .attach(journalContainer)
                                .commitNow();
                        break;
                    case "MoreKey":
                        fm.beginTransaction()
                                .detach(journalContainer)
                                .detach(mainContainer)
                                .attach(settingsContainer)
                                .commitNow();
                        break;
                }

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        App.appComponent.inject(MainActivity.this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initContainers();
        BottomNavigationView bottomNavigationView = findViewById(R.id.navMain);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        BottomNavigationHelper.setupIcons(bottomNavigationView);

        bp = new BillingProcessor(this, getString(R.string.license_key), this);
        bp.initialize();

        if (savedInstanceState == null) {
            routerMain.replaceScreen(ScreenKeys.Main.getValue(), 1);
        }
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        navigatorHolder.removeNavigator();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (bp != null) {
            bp.release();
        }

        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.flMain);
        if (fragment != null && (fragment instanceof BackButtonListener) && ((BackButtonListener) fragment).onBackPressed()) {
            // Do nothing
        } else {
            mainPresenter.onBackClick();
        }
    }

    /**
     * Containers arch selected because of bottom menu navigation
     * Containers is subNavigation for saving transport logic of each tab
     */
    private void initContainers() {
        FragmentManager fm = getSupportFragmentManager();
        mainContainer = (MainContainer) fm.findFragmentByTag(mainTAG);
        if (mainContainer == null) {
            mainContainer = MainContainer.Companion.getNewInstance(mainTAG);
            fm.beginTransaction()
                    .add(R.id.flMain, mainContainer, mainTAG)
                    .detach(mainContainer)
                    .commitNow();
        }

        journalContainer = (JournalContainer) fm.findFragmentByTag(journalTAG);
        if (journalContainer == null) {
            journalContainer = JournalContainer.Companion.getNewInstance(journalTAG);
            fm.beginTransaction()
                    .add(R.id.flMain, journalContainer, journalTAG)
                    .detach(journalContainer)
                    .commitNow();
        }

        settingsContainer = (SettingsContainer) fm.findFragmentByTag(settingsTag);
        if (settingsContainer == null) {
            settingsContainer = SettingsContainer.Companion.getNewInstance(settingsTag);
            fm.beginTransaction()
                    .add(R.id.flMain, settingsContainer, settingsTag)
                    .detach(settingsContainer)
                    .commitNow();
        }
    }

    // MARK: - Router provider implementation
    @NotNull
    @Override
    public Router getRouter() {
        return routerMain;
    }

    // MARK: - Keyboard handler implementation
    @Override
    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    @Override
    public void showKeyboard() {

    }

    // MARK: - Fragment view implementation
    @Override
    public void navigateTo(@NotNull String screenKey, @Nullable Bundle data) {
        routerMain.replaceScreen(screenKey, data);
    }

    @Override
    public void updateActionBar(boolean isBack, boolean isHistory, @NotNull String title) {
        findViewById(R.id.btnBack).setVisibility(isBack ? View.VISIBLE : View.INVISIBLE);
        findViewById(R.id.tbMain).setVisibility(isHistory ? View.VISIBLE : View.GONE);
        ((TextView) findViewById(R.id.txtTbTitle)).setText(title);
    }

    @Override
    public void exitApp() {
        finish();
    }

    // MARK: - View implementation
    @Override
    public void performDoubleTap(int i) {
        switch (i) {
            case 0:
                mainContainer.doubleTap();
                break;

            case 1:
                journalContainer.doubleTap();
                break;

            case 2:
                settingsContainer.doubleTap();
                break;
        }
    }

    // MARK: - IBilling Implementation
    @Override
    public void onProductPurchased(@NonNull String productId, @android.support.annotation.Nullable TransactionDetails details) {

    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, @android.support.annotation.Nullable Throwable error) {

    }

    @Override
    public void onBillingInitialized() {

    }

    // MARK: - Billing Provider implementation
    public BillingProcessor getBilling() {
        return bp;
    }
}
