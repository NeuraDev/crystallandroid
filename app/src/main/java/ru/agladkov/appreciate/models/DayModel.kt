package ru.agladkov.appreciate.models

/**
 * Created by neura on 09.03.18.
 */
data class DayModel(val content: String, val sectionHeader: String)