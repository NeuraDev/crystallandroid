package ru.agladkov.appreciate.models

/**
 * Created by neura on 09.03.18.
 */
data class JournalModel(val id: Int, val title: String, var sectionHeader: String,
                        var recordsCount: Int, val year: Int, val month: Int)