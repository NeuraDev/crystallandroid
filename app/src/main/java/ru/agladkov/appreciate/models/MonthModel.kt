package ru.agladkov.appreciate.models

/**
 * Created by neura on 09.03.18.
 */
data class MonthModel(val id: Int, val dayNumber: Int, val monthName: String,
                      val recordsCount: Int, val percent: Int)