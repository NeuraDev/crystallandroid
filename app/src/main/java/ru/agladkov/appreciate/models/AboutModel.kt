package ru.agladkov.appreciate.models

/**
 * Created by neura on 09.03.18.
 */
data class AboutModel(val id: Int, val title: String)