package ru.agladkov.appreciate.models

/**
 * Created by neura on 09.03.18.
 */
data class SwitchModel(val id: Int, val title: String, var isOn: Boolean)