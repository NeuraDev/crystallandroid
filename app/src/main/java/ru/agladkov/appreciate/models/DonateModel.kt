package ru.agladkov.appreciate.models

/**
 * Created by neura on 09.03.18.
 */
data class DonateModel(val id: String, val title: String, val cost: Double,
                       val currency: String, val description: String)